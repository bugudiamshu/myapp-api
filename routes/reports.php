<?php

use App\Http\Controllers\ReportsController;
use App\Http\Middleware\AdminUnauthorizedAccessRoute;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => 'payments',
        'middleware' => OperatorUnauthorizedAccessRoute::class
    ],
    function () {
        Route::get(
            '/section/{section_id}',
            [ ReportsController::class,  'showSectionWisePayments']
        )->name('payments.section-wise');
        
        Route::get(
            '/class/{class_id}',
            [ ReportsController::class,  'showClassWisePayments']
        )->name('payments.class-wise');
        
        Route::get(
            '/course/{course_id}',
            [ ReportsController::class,  'showCourseWisePayments']
        )->name('payments.course-wise');
    }
);
