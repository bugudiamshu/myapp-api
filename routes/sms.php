<?php

use App\Http\Controllers\ReportsController;
use App\Http\Controllers\SmsController;
use App\Http\Middleware\AdminUnauthorizedAccessRoute;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group([],
    function () {
        Route::post(
            '/section_wise',
            [ SmsController::class,  'sectionWiseFeeReminder']
        )->name('sms.section-wise');
    }
);
