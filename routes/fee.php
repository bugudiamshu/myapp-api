<?php

use App\Http\Controllers\FeeController;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => OperatorUnauthorizedAccessRoute::class
    ],
    function () {
        Route::put(
            '/student_fee/{student_fee_id}',
            [ FeeController::class, 'update' ]
        )->name('student_fee.update');
    }
);

Route::get(
    '/class/{section_id}',
    [ FeeController::class, 'show' ]
)->name('class_fee.show');

Route::get(
    '/student/{student_id}',
    [ FeeController::class, 'showStudentFee' ]
)->name('student_fee.show');
