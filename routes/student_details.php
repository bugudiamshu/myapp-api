<?php

use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Route;

/* Student Additional Info APIs */
Route::get('/fields', [
    StudentController::class, 'getFields'
])->name('fields');

Route::get(
    '/basic/{student_id}',
    [ StudentController::class, 'getStudentBasicInfo' ]
)->name('basic.show');

Route::get(
    '/{student_id}',
    [ StudentController::class, 'getStudentDetails' ]
)->name('show');

Route::post(
    '/joining',
    [ StudentController::class, 'saveJoiningDetails' ]
)->name('joining.create');

Route::post(
    '/personal',
    [ StudentController::class, 'savePersonalDetails' ]
)->name('personal.create');

Route::post(
    '/other',
    [ StudentController::class, 'saveOtherDetails' ]
)->name('other.create');

Route::post(
    '/removal',
    [ StudentController::class, 'saveRemovalDetails' ]
)->name('removal.create');
