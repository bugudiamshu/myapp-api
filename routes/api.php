<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['cors']], function () {
    Route::post(
        'v1/login',
        'Auth\LoginController@authenticate'
    );
});

Route::group(['middleware' => ['jwtAuthentication']], function () {
    Route::group(['prefix' => 'v1'], function () {

        Route::name('common.')
            ->group(__DIR__ . '/common.php');

        Route::name('students.')
            ->prefix('students')
            ->group(__DIR__ . '/student.php');

        Route::name('student_details.')
            ->prefix('student_details')
            ->group(__DIR__ . '/student_details.php');

        Route::name('fee.')
            ->prefix('fee')
            ->group(__DIR__ . '/fee.php');

        Route::name('payments.')
            ->prefix('payments')
            ->group(__DIR__ . '/payments.php');

        Route::name('alumni.')
            ->prefix('alumni')
            ->group(__DIR__ . '/alumni.php');

        Route::name('reports.')
            ->prefix('reports')
            ->group(__DIR__ . '/reports.php');

        Route::name('attendance.')
            ->prefix('attendance')
            ->group(__DIR__ . '/attendance.php');

        Route::name('print.')
            ->prefix('print')
            ->group(__DIR__ . '/print.php');

        Route::name('transactions.')
            ->prefix('transactions')
            ->group(__DIR__ . '/transactions.php');

        Route::name('sms.')
            ->prefix('sms')
            ->group(__DIR__ . '/sms.php');
    });
});
