<?php

use App\Http\Controllers\ClassController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\FeeController;
use App\Http\Controllers\SectionController;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => OperatorUnauthorizedAccessRoute::class
    ],
    function () {
        Route::post(
            'classes/promote',
            [ClassController::class,  'promoteClass']
        )->name('transfer');
    }
);

Route::get(
    'courses/',
    [CourseController::class, 'index']
)->name('courses.list');

Route::get(
    'courses/section/{section_id}',
    [CourseController::class, 'getCourseIdBySection']
)->name('courses_by_section');

Route::get(
    'classes/{course_id}',
    [ClassController::class,  'getClassesByCourseId']
)->name('classes.list');

Route::get(
    'sections/{class_id}',
    [SectionController::class, 'getSectionsByClassId']
)->name('sections.list');

Route::get(
    'fee_types/{student_id}',
    [FeeController::class, 'getFeeTypes']
)->name('fee_types');

Route::get(
    'fee_types/',
    [FeeController::class, 'index']
)->name('fee_types.list');

Route::post(
    'logout',
    'Auth\LoginController@logout'
)->name('logout');

Route::get(
    'courses/all',
    [CourseController::class, 'getAllCourses']
);
