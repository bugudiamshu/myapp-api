<?php

use App\Http\Controllers\PrintController;
use App\Http\Middleware\AdminUnauthorizedAccessRoute;
use App\Http\Middleware\InchargeUnauthorizedAccessRoute;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => [
            AdminUnauthorizedAccessRoute::class,
            OperatorUnauthorizedAccessRoute::class,
            InchargeUnauthorizedAccessRoute::class
        ]
    ],
    function () {
        Route::get(
            '/section_wise_fee/{section_id}/{fee_type_id}',
            [ PrintController::class, 'printSectionWiseFee']
        );
        
        Route::get(
            '/class_wise_fee/{class_id}/{fee_type_id}',
            [ PrintController::class, 'printClassWiseFee']
        );
        
        Route::get(
            '/course_wise_fee/{course_id}/{fee_type_id}',
            [ PrintController::class, 'printCourseWiseFee']
        );

        Route::post(
            '/transactions',
            [ PrintController::class, 'printTransactions']
        );
    }
);

Route::get(
    '/payments/{student_id}/{fee_type_id}',
    [ PrintController::class, 'printPayments']
);

Route::post(
    '/section_wise_students_list',
    [ PrintController::class, 'printSectionWiseStudents']
);

Route::get(
    '/bonafide/{student_id}',
    [ PrintController::class, 'printBonafide' ]
)->middleware(OperatorUnauthorizedAccessRoute::class);
