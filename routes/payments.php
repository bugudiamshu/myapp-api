<?php

use App\Http\Controllers\PaymentController;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => OperatorUnauthorizedAccessRoute::class
    ],
    function () {
        Route::delete(
            '/{payment_id}',
            [ PaymentController::class, 'delete' ],
        )->name('delete');
    }
);

Route::post(
    '/',
    [ PaymentController::class, 'create' ]
)->name('create');

Route::get(
    '/student/{student_id}',
    [ PaymentController::class, 'getPaymentsByStudentId' ]
)->name('filter_by.student_id');

Route::get(
    '/receipt/{receipt_number}',
    [ PaymentController::class, 'getPaymentsByReceiptNumber' ],
)->name('filter_by.receipt_number');

Route::get(
    '/previous/{student_id}/{fee_type_id}',
    [ PaymentController::class, 'previousPayments' ],
)->name('previous.show');

Route::get(
    'previous/{student_id}',
    [ PaymentController::class, 'previousPayments' ],
)->name('previous.show');

Route::put(
    '/{payment_id}',
    [ PaymentController::class, 'update' ],
)->name('update');
