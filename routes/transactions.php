<?php

use App\Http\Controllers\TransactionController;
use App\Http\Middleware\AdminUnauthorizedAccessRoute;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => OperatorUnauthorizedAccessRoute::class
    ],
    function () {
        Route::post(
            '/filter',
            [ TransactionController::class, 'filter']
        );

        Route::delete(
            '/{id}',
            [ TransactionController::class, 'delete']
        )->middleware(AdminUnauthorizedAccessRoute::class);

        Route::post(
            '/{id}',
            [ TransactionController::class, 'update']
        );
    }
);

Route::post(
    '/',
    [ TransactionController::class, 'create']
);

Route::get(
    '/tags',
    [ TransactionController::class, 'tags']
);

Route::get(
    '/{id}',
    [ TransactionController::class, 'show']
);
