<?php

use App\Http\Controllers\StudentController;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::get('/', [
    StudentController::class, 'index'
])->name('list');

Route::post('/', [
    StudentController::class, 'create'
])->name('create');

Route::get('/{student_id}', [
    StudentController::class, 'show'
])->name('student.show');

Route::put('/{student_id}', [
    StudentController::class, 'edit'
])->name('edit');

Route::delete('/{student_id}', [
    StudentController::class, 'delete'
])->name('delete')->middleware(OperatorUnauthorizedAccessRoute::class);

Route::get('/search/{student_name}', [
    StudentController::class, 'search'
])->name('search');

Route::get('/section/{section_id}', [
    StudentController::class, 'getStudentsBySectionId'
])->name('filter_by.sectionId');

Route::get('/basic_info/{section_id}', [
    StudentController::class, 'getStudentsBasicInfoBySectionId'
])->name('basic_info.filter_by.sectionId');

Route::post('/uploadImage', [
    StudentController::class, 'upload'
])->name('student.upload_image');
