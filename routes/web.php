<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'signed'], function () {
    Route::get(
        '/generateSectionWiseFee/{user_id}/{section_id}/{fee_type_id}',
        'PDFController@generateSectionWiseFeePDF'
    )->name('generateSectionWiseFee');

    Route::get(
        '/classWiseFee/{user_id}/{class_id}/{fee_type_id}',
        'PDFController@generateClassWiseFeePDF'
    )->name('generateClassWiseFee');

    Route::get(
        '/courseWiseFee/{user_id}/{course_id}/{fee_type_id}',
        'PDFController@generateCourseWiseFeePDF'
    )->name('generateCourseWiseFee');

    Route::get(
        '/printPaymentsReceipt/{user_id}/{student_id}/{fee_type_id}',
        'PDFController@generatePaymentsPDF'
    )->name('generatePaymentsReceipt');

    Route::get(
        '/generateTransactions/{user_id}/{from_date}/{to_date}/{type}/{tag}',
        'PDFController@generateTransactionsPDF'
    )->name('generateTransactions');

    Route::get(
        '/generateSectionWiseStudentsList/{user_id}/{section_ids}/{column_names}',
        'PDFController@generateSectionWiseStudentsListPDF'
    )->name('generateSectionWiseStudentsList');

    Route::get(
        '/generateBonafide/{user_id}/{student_id}',
        'PDFController@generateBonafidePDF'
    )->name('generateBonafide');
});
