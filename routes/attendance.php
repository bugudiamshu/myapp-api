<?php

use App\Http\Controllers\AttendanceController;
use App\Http\Middleware\OperatorUnauthorizedAccessRoute;
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'middleware' => OperatorUnauthorizedAccessRoute::class
    ],
    function () {
        Route::delete(
            '/{attendance_id}/{section_id}',
            [ AttendanceController::class, 'delete']
        )->name('delete');
    }
);

Route::get(
    '/sessions',
    [ AttendanceController::class, 'getSessions']
)->name('sessions');

Route::get(
    '/students/{section_id}',
    [ AttendanceController::class, 'getStudents']
)->name('students');

Route::get(
    '/students/{section_id}/{session_id}/{date}',
    [ AttendanceController::class, 'getAttendance']
)->name('students.show');

Route::post(
    '/',
    [ AttendanceController::class, 'create']
)->name('create');
