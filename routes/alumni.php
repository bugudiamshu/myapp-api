<?php

use App\Http\Controllers\AlumniController;
use Illuminate\Support\Facades\Route;

Route::get(
    '/batches',
    [ AlumniController::class,  'getBatches'],
)->name('batches');

Route::get(
    '/courses/{batch}',
    [ AlumniController::class,  'getCourses'],
)->name('courses');

Route::get(
    '/classes/{batch}/{course_id}',
    [ AlumniController::class,  'getClasses'],
)->name('classes');

Route::get(
    '/{batch}/{class_id}',
    [ AlumniController::class,  'getAlumni'],
)->name('byBatch.byClass');

Route::get(
    '/search/{student_name}',
    [ AlumniController::class,  'searchAlumni'],
)->name('search');

Route::post(
    '/',
    [ AlumniController::class,  'create'],
)->name('create');
