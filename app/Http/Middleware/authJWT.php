<?php

namespace app\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Http\Middleware\Authenticate;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use JWTAuth;

class AuthJWT extends Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (! $oldtoken = $this->auth->setRequest($request)->getToken()) {
            return response()->json([
                    'error' => 'Token not provided'
                ], 400);
        }
        try {
            $token = $oldtoken;
            JWTAuth::setToken($oldtoken)->toUser();
            $user = JWTAuth::authenticate($oldtoken);
        } catch (TokenExpiredException $e) {
            // If the token is expired, then it will be refreshed and added to the headers
            try {
                $token = JWTAuth::refresh($oldtoken);
                JWTAuth::setToken($token)->toUser();
                $user = JWTAuth::authenticate($token);
            } catch (JWTException $e) {
                return response()->json([
                    'error' => 'Unauthorized'
                ], 401)->withHeaders([
                        'Authorization' => $oldtoken
                    ]);
            }
        } catch (JWTException $e) {
            return response()->json([
                    'error' => 'Please provide the correct token'
                ], 401)->withHeaders([
                        'Authorization' => $oldtoken
                    ]);
        }

        if (!$user) {
            return response()->json([
                    'error' => 'User not Found'
                ], 404)->withHeaders([
                        'Authorization' => $oldtoken
                    ]);
        }
        return $next($request)->withHeaders([
                'Authorization' => $token
            ]);
    }
}
