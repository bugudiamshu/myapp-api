<?php

namespace App\Http\Middleware;

use App\Enums\RoleEnum;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminUnauthorizedAccessRoute
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->hasRole(RoleEnum::admin()->value)) {
            return response()->json([
                'error' => 'Unauthorized access to the route',
            ], 403);
        }

        return $next($request);
    }
}
