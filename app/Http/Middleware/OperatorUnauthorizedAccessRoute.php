<?php

namespace App\Http\Middleware;

use App\Enums\RoleEnum;
use Closure;

class OperatorUnauthorizedAccessRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->hasRole(RoleEnum::operator()->value)) {
            return response()->json([
                'error' => 'Unauthorized access to the route'
            ], 401);
        }

        return $next($request);
    }
}
