<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required|exists:students,id',
            'receipt_number' => 'required',
            'amount' => 'required|numeric',
            'paid_by' => 'required|max:255|regex:/^[a-zA-Z ]*$/',
            'collected_by' => 'required|max:255|regex:/^[a-zA-Z ]*$/',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'Required',
            'amount.numeric' => 'Amount should be numeric',
            'paid_by.regex' => 'Paid by should contain only alphabets and spaces',
            'collected_by.regex' => 'Collected by should contain only alphabets and spaces',
        ];
    }
}
