<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\PrintService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class PrintController extends Controller
{
    /**
     * Send the web route to generate Print section wise fee PDF
     *
     * @param int $section_id
     * @param int $fee_type_id
     * @return JsonResponse
     */
    public function printSectionWiseFee($section_id, $fee_type_id)
    {
        $user = Auth::user();
        $url = URL::temporarySignedRoute('generateSectionWiseFee', now()->addMinute(10), [
            'user_id'       => $user->id,
            'section_id'    => $section_id,
            'fee_type_id'   => $fee_type_id
        ]);

        return response()->json(['url' => $url], 200);
    }

    /**
     * Send the web route to generate Print class wise fee PDF
     *
     * @param int $class_id
     * @param int $fee_type_id
     * @return JsonResponse
     */
    public function printClassWiseFee($class_id, $fee_type_id)
    {
        $user = Auth::user();
        $url = URL::temporarySignedRoute('generateClassWiseFee', now()->addMinute(10), [
            'user_id'       => $user->id,
            'class_id'      => $class_id,
            'fee_type_id'   => $fee_type_id
        ]);

        return response()->json(['url' => $url], 200);
    }

    /**
     * Send the web route to generate Print class wise fee PDF
     *
     * @param int $class_id
     * @param int $fee_type_id
     * @return JsonResponse
     */
    public function printCourseWiseFee($course_id, $fee_type_id)
    {
        $user = Auth::user();
        $url = URL::temporarySignedRoute('generateCourseWiseFee', now()->addMinute(10), [
            'user_id'       => $user->id,
            'course_id'      => $course_id,
            'fee_type_id'   => $fee_type_id
        ]);

        return response()->json(['url' => $url], 200);
    }

    /**
     * Send the web route to generate Payments PDF
     *
     * @param int $student_id
     * @param int $fee_type_id
     * @return JsonResponse
     */
    public function printPayments($student_id, $fee_type_id)
    {
        $user = Auth::user();
        $url = URL::temporarySignedRoute('generatePaymentsReceipt', now()->addMinute(10), [
            'user_id'       => $user->id,
            'student_id'    => $student_id,
            'fee_type_id'   => $fee_type_id
        ]);

        return response()->json(['url' => $url], 200);
    }

    /**
     * Send the web route to generate Transactions PDF
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function printTransactions(Request $request)
    {
        $user = Auth::user();
        $url = URL::temporarySignedRoute('generateTransactions', now()->addMinute(10), [
            'user_id'       => $user->id,
            'from_date'     => $request->from_date,
            'to_date'       => $request->to_date,
            'type'          => $request->type,
            'tag'           => $request->tag
        ]);

        return response()->json(['url' => $url], 200);
    }

    /**
     * Send the web route to generate Transactions PDF
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function printSectionWiseStudents(Request $request)
    {
        $user = Auth::user();
        $url = URL::temporarySignedRoute('generateSectionWiseStudentsList', now()->addMinute(10), [
            'user_id'       => $user->id,
            'section_ids'   => urlencode(serialize($request->section_ids)),
            'column_names'  => urlencode(serialize($request->column_names)),
        ]);

        return response()->json(['url' => $url], 200);
    }

    /**
     * Send the web route to generate Bonafide PDF
     *
     * @param int $student_id
     * @return JsonResponse
     */
    public function printBonafide($student_id)
    {
        $user = Auth::user();
        $url = URL::temporarySignedRoute('generateBonafide', now()->addMinute(10), [
            'user_id'       => $user->id,
            'student_id'    => $student_id
        ]);

        return response()->json(['url' => $url], 200);
    }
}
