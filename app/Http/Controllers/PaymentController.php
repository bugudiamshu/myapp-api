<?php

namespace App\Http\Controllers;

use App\Exceptions\PaymentException;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Models\StudentPayment;
use App\Services\PaymentService;
use App\Services\SMSService;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    private $statusCode = 200;

    /**
     * Create Payment
     *
     * @param CreatePaymentRequest $request
     * @return JsonResponse
     */
    public function create(CreatePaymentRequest $request)
    {
        try {
            $paymentService = new PaymentService();
            if ($paymentService->validateCreatePayment($request->student_id, $request->fee_type_id, $request->amount)) {
                $payment = $paymentService->createPayment($request);
                $response['message'] = config('constants.MESSAGES.CREATED');
                $response['payment_id'] = $payment->id;
                SMSService::sendPaymentAcknowledgement($payment);
            }
        } catch (PaymentException $e) {
            Log::error("Could not create Payment (create) : " . $e->getMessage());
            $response['error'] = $e->getMessage();
            $this->statusCode = 400;
        } catch (Exception $e) {
            Log::error("Could not create Payment (create) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Previous payments of a Student
     *
     * @param int $student_id
     * @param int $fee_type_id
     * @return JsonResponse
     */
    public function previousPayments($student_id, $fee_type_id = null)
    {
        try {
            $response = (new PaymentService())->previousPayments($student_id, $fee_type_id);
        } catch (Exception $e) {
            Log::error("Could not fetch Previous Payments (previousPayments) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Update Payment
     *
     * @param int $payment_id
     * @param UpdatePaymentRequest $request
     * @return JsonResponse
     */
    public function update($payment_id, UpdatePaymentRequest $request)
    {
        try {
            $payment = StudentPayment::findOrFail($payment_id);
            if ($request->fee_type_id != $payment->fee_type_id) {
                $payment->paid_amount = 0;
            }
            $payment_service = new PaymentService();
            if ($payment_service->validateUpdatePayment($payment, $request->fee_type_id, $request->amount)) {
                $payment_service->updatePayment($payment, $request);
                $response['message'] = config('constants.MESSAGES.UPDATED');
                $response['payment_id'] = $payment->id;
            }
        } catch (PaymentException $e) {
            Log::error("Could not create Payment (create) : " . $e->getMessage());
            $response['error'] = $e->getMessage();
            $this->statusCode = 400;
        } catch (Exception $e) {
            Log::error("Could not Update Payment (update) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Delete Payment
     *
     * @param int $payment_id
     * @return JsonResponse
     */
    public function delete($payment_id)
    {
        try {
            StudentPayment::find($payment_id)->delete();
            $response['message'] = config('constants.MESSAGES.DELETED');
        } catch (Exception $e) {
            Log::error("Could not delete Payment (delete) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Payments by Receipt Number
     *
     * @param string $receipt_number
     * @return Collection|[]
     */
    public function getPaymentsByReceiptNumber($receipt_number)
    {
        try {
            $response = StudentPayment::where('receipt_number', $receipt_number)
                ->where('branch_id', Auth::user()->branch_id)->get()->each(function ($payment) {
                    return $payment->iso_receipt_date = Carbon::createFromFormat(
                        'Y-m-d',
                        $payment->receipt_date
                    )->toISOString();
                });
        } catch (Exception $e) {
            Log::error("Could not get payments by receipt number (getPaymentsByReceiptNumber) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }
}
