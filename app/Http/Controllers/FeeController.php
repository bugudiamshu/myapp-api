<?php

namespace App\Http\Controllers;

use App\Models\ClassFee;
use App\Models\FeeType;
use App\Models\Section;
use App\Models\Student;
use App\Models\StudentFee;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FeeController extends Controller
{
    private $statusCode = 200;

    /**
     * Get All Fee Types for branch
     *
     * @return ResponseJson
     */
    public function index()
    {
        try {
            $response = FeeType::where('branch_id', Auth::user()->branch_id)->get();
        } catch (Exception $e) {
            Log::error("Get all Fee types (getAllFeeTypes) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }
        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Class Fee by Section ID
     *
     * @param int $section_id
     * @return Collection|[]
     */
    public function show($section_id)
    {
        try {
            $class_id = Section::find($section_id)->class_id;
            $response = ClassFee::where('class_id', $class_id)->get();
        } catch (Exception $e) {
            Log::error("Show Class Fee (show) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }
    
        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Student Fee by Student ID
     *
     * @param int $student_id
     * @return JsonResponse
     */
    public function showStudentFee($student_id)
    {
        try {
            $student_fee = StudentFee::where('student_id', $student_id)->get()->toArray();
            $response = [
                'student_fee' => $student_fee,
                'total_fee' => array_sum(array_column($student_fee, 'amount'))
            ];
        } catch (Exception $e) {
            Log::error("Get Student Fee (showStudentFee) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }
        
        return response()->json($response, $this->statusCode);
    }

    /**
     * Update Student Fee
     *
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update($id, Request $request)
    {
        try {
            $student_fee = StudentFee::findOrFail($id);
            $student_fee->amount = $request->amount;
            $student_fee->save();
            $student_fee = StudentFee::where('student_id', $request->student_id)->get()->toArray();
            $response = [
                'student_fee' => $student_fee,
                'total_fee' => array_sum(array_column($student_fee, 'amount'))
            ];
        } catch (Exception $e) {
            Log::error("Update Student Fee (update) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }
        
        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Fee Types by Student ID
     *
     * @param int $student_id
     * @return StudentFee
     */
    public function getFeeTypes($student_id)
    {
        try {
            $student = Student::findOrFail($student_id);
            $response = $student->studentFee;
        } catch (Exception $e) {
            Log::error("Get Fee types (getFeeTypes) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }
    
        return response()->json($response, $this->statusCode);
    }
}
