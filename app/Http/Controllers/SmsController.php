<?php

namespace App\Http\Controllers;

use App\Services\SMSService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SmsController extends Controller
{
    public function sectionWiseFeeReminder(Request $request)
    {
        try {
            foreach ($request->all() as $data) {
                SMSService::sendFeeReminderSMS($data['studentName'], $data['mobileNumber'], $data['pendingFee']);
            }
            $statusCode = 200;
            $response['message'] = 'SMS Successfully sent';
        } catch (\Exception $exception) {
            $response['error'] = config('constants.ERROR.500');
            $statusCode = 500;
            Log::debug('Section Wise Fee Reminder Failed due to : ' . $exception->getMessage());
        }

        return response()->json($response, $statusCode);
    }
}
