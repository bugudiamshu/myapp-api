<?php

namespace App\Http\Controllers;

use App\Enums\TransactionTypeEnum;
use App\Jobs\SendSMS;
use App\Models\Transaction;
use App\Services\FileUploadService;
use App\Services\PrintService;
use App\Services\SMSService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    private $statusCode = 200;

    /**
     * Create Transaction
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $user = Auth::user();
            $transaction = Transaction::create([
                'branch_id'         => $user->branch_id,
                'transaction_type'  => $request->transaction_type,
                'transaction_date'  => date('Y-m-d', strtotime($request->transaction_date)),
                'amount'            => $request->amount,
                'purpose'           => $request->purpose,
                'tag'               => $request->tag == 'null' ? null : $request->tag
            ]);

            if ($transaction && $request->file('receipt_file')) {
                $transaction_file = $request->file('receipt_file');
                $transaction = (new FileUploadService())->upload($transaction, $transaction_file, 'transaction_files');
            }

            if (TransactionTypeEnum::make($request->transaction_type)->equals(TransactionTypeEnum::income())) {
                SMSService::sendIncomeSMS($user, $transaction);
            } else {
                logger('exp');
                SMSService::sendExpenditureSMS($user, $transaction);
            }

            $response = [
                'id'        => $transaction->id,
                'message'   => config('constants.MESSAGES.CREATED')
            ];
        } catch (Exception $e) {
            Log::error("Create Transaction (create) : " . $e->getMessage());
            Log::error("Line Number : " . $e->getLine());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Filter Transactions
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function filter(Request $request)
    {
        try {
            $user = Auth::user();
            $date_range = [
                date('Y-m-d', strtotime($request->from_date)),
                date('Y-m-d', strtotime($request->to_date))
            ];
            $response = (new PrintService())->setUser($user)->filterTransactions(
                $request->type,
                $date_range,
                $request->tag
            );
        } catch (Exception $e) {
            Log::error("Filter Transactions (filter) : " . $e->getMessage());
            Log::error("Line Number : " . $e->getLine());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Transaction by ID
     *
     * @param int $id
     * @return Transaction
     */
    public function show($id)
    {
        return Transaction::find($id);
    }

    /**
     * Update Transaction
     *
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $transaction = Transaction::findOrFail($id);
            $data = [
                'transaction_type'  => $request->transaction_type,
                'transaction_date'  => date('Y-m-d', strtotime($request->transaction_date)),
                'amount'            => $request->amount,
                'purpose'           => $request->purpose,
                'tag'               => $request->tag
            ];

            if ($transaction->update($data)) {
                if ($request->file('receipt_file')) {
                    $transaction_file = $request->file('receipt_file');
                    $transaction = (new FileUploadService())->upload(
                        $transaction,
                        $transaction_file,
                        'transaction_files'
                    );
                }
                $response['message'] = config('constants.MESSAGES.UPDATED');
            }
        } catch (Exception $e) {
            Log::error("Update Transaction (update) : " . $e->getMessage());
            Log::error("Line Number : " . $e->getLine());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Delete Transaction
     *
     * @param int $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        try {
            Transaction::find($id)->delete();
            $response['message'] = config('constants.MESSAGES.DELETED');
        } catch (Exception $e) {
            Log::error("Could not delete Transaction (delete) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get all transaction tags
     *
     * @return void
     */
    public function tags()
    {
        try {
            $tags = Transaction::whereNotNull('tag')->pluck('tag')->unique()->values();
        } catch (Exception $e) {
            Log::error("Could not get Transaction tags (tags) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($tags, $this->statusCode);
    }
}
