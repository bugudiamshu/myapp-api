<?php

namespace App\Http\Controllers;

use App\Http\Resources\ClassWiseFeeReport;
use App\Models\Clas;
use App\Models\ClassFee;
use App\Models\Course;
use App\Models\Section;
use App\Models\Student;
use App\Models\StudentFee;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ReportsController extends Controller
{
    private $statusCode = 200;

    /**
     * Show Payments by SectionWise
     *
     * @param int $section_id
     * @return JsonReponse
     */
    public function showSectionWisePayments($section_id)
    {
        try {
            $section = Section::findOrFail($section_id);
            $students = $section->students()->with(['studentFee'])->get();
            $response['section_payments'] = $students;
        } catch (Exception $e) {
            Log::error("Show section wise payments (showSectionWisePayments) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }
        

        return response()->json($response, $this->statusCode);
    }

    /**
     * Show Payments by ClassWise
     *
     * @param int $class_id
     * @return JsonResponse
     */
    public function showClassWisePayments($class_id)
    {
        try {
            $class = Clas::findOrFail($class_id);
            $sections = $class->sections->toArray();
            $response['section_wise_details'] = $sections;
        } catch (Exception $e) {
            Log::error("Show class wise payments (showClassWisePayments) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Show Payments by CourseWise
     *
     * @param int $course_id
     * @return JsonResponse
     */
    public function showCourseWisePayments($course_id)
    {
        try {
            $course = Course::findOrFail($course_id);
            $classes = $course->classes()->get();
            $response['class_wise_details'] = ClassWiseFeeReport::collection($classes);
        } catch (Exception $e) {
            Log::error("Show course wise payments (showCourseWisePayments) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }
        
        return response()->json($response, $this->statusCode);
    }
}
