<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;

class ConfigurationController extends Controller
{
    private const ERROR_PERMISSIONS = 'User doesn\'t have Permissions';

    public function createOrganisation()
    {
        if (!Auth::user()->can('Create Organisation')) {
            return response()->json([
                'status' => 403,
                'message' => self::ERROR_PERMISSIONS,
            ], 403);
        }

        return response()->json([
            'status' => 200,
        ]);
    }

    public function test()
    {
        if (!Auth::user()->can('Create Student')) {
            return response()->json([
                'status' => 403,
            ]);
        }

        return response()->json([
            'status' => 200,
        ]);
    }
}
