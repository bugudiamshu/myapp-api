<?php

namespace App\Http\Controllers;

use App\Models\Alumni;
use App\Models\Clas;
use App\Models\Course;
use App\Models\Student;
use App\Models\User;
use App\Services\AlumniService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AlumniController extends Controller
{
    private $statusCode = 200;
    /**
     * Create Alumni
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $student = Student::findOrFail($request->student_id);
            $alumniService = new AlumniService();
            $alumniService->createAlumni($student, $request->batch);
            $alumniService->deactivateAlumni($student);
            $response['message'] = config('constants.MESSAGES.CREATED');
        } catch (Exception $e) {
            Log::error("Could not create Alumni (create) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Alumni Batches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBatches()
    {
        try {
            $response = Alumni::all()
                ->where('branch_id', Auth::user()->branch_id)
                ->unique('batch')->pluck('batch');
        } catch (Exception $e) {
            Log::error("Could not get Batches (getBatches) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Course by Batch
     *
     * @param string $batch
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCourses($batch)
    {
        try {
            $alumni = Alumni::where('batch', $batch)
                ->where('branch_id', Auth::user()->branch_id)->get();
            $response = (new AlumniService())->getCourses($alumni);
        } catch (Exception $e) {
            Log::error("Could not get Courses for Alumni (getCourses) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Alumni classes
     *
     * @param string $batch
     * @param int $course_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClasses($batch, $course_id)
    {
        try {
            $alumni = Alumni::where('branch_id', Auth::user()->branch_id)
                ->where('batch', $batch)
                ->where('course_of_removal_id', $course_id)
                ->get();
            $response = (new AlumniService())->getClasses($alumni);
        } catch (Exception $e) {
            Log::error("Could not get Classes for Alumni (getClasses) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Alumni Students List
     *
     * @param string $batch
     * @param int $class_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAlumni($batch, $class_id)
    {
        try {
            $response = (new AlumniService())->getAlumni($batch, $class_id);
        } catch (Exception $e) {
            Log::error("Could not get Alumni List (getAlumni) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Search Alumni using name
     *
     * @param string $student_name
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchAlumni($student_name)
    {
        try {
            $response = Student::where('student_name', 'LIKE', '%' . $student_name . '%')
                ->where('branch_id', Auth::user()->branch_id)
                ->where('is_alumni', true)
                ->with('studentDetails')
                ->get();
        } catch (Exception $e) {
            Log::error("Could not Search Alumni (searchAlumni) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }
}
