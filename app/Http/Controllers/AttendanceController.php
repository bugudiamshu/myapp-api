<?php

namespace App\Http\Controllers;

use App\Models\Session;
use App\Models\Student;
use App\Services\AttendanceService;
use App\Services\SMSService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class AttendanceController extends Controller
{
    private $statusCode = 200;

    /**
     * Get Sessions for the attendance
     *
     * @return JsonResponse
     */
    public function getSessions()
    {
        try {
            $response = Session::where('branch_id', Auth::user()->branch_id)->get();
        } catch (Exception $e) {
            Log::error("Could not get sessions (getSessions) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Students by Section ID
     *
     * @param int $section_id
     * @return JsonResponse
     */
    public function getStudents($section_id)
    {
        try {
            $response = Student::where('is_alumni', false)
                ->where('section_id', $section_id)
                ->get();
        } catch (Exception $e) {
            Log::error("Could not get Students (getStudents) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Save Attendance for Students
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $attendance_service = new AttendanceService();
            $student_attendance = $attendance_service->setSessionId($request->session_id)
                ->setAbsentIds($request->absent_ids)
                ->setSectionId($request->section_id)
                ->setDate(date('Y-m-d', strtotime($request->date)))
                ->createAttendance();


            if (!$student_attendance->wasRecentlyCreated) {
                $attendance_service->updateAttendance($student_attendance);
            }

            $session = Session::find($request->session_id);
            foreach ($request->absent_ids as $studentId) {
                $student = Student::find($studentId);
                SMSService::sendDailyAttendanceSMS($student->student_name, $session->name, $student->mobile_number);
            }

            $response['id'] = $student_attendance->id;
            $response['message'] = 'Attendance Marked Successfully';
        } catch (Exception $e) {
            Log::error("Could not create attendance (create) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }

        return response()->json($response, $this->statusCode);
    }


    /**
     * Get Student Attendance
     *
     * @param int $section_id
     * @param int $session_id
     * @param Date $date
     * @return JsonResponse
     */
    public function getAttendance($section_id, $session_id, $date)
    {
        $response = [];

        try {
            $response = (new AttendanceService())->setSectionId($section_id)
                                            ->setDate($date)
                                            ->setSessionId($session_id)
                                            ->getAttendance();

        } catch (Exception $e) {
            Log::error("Could not get attendance (getAttendance) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Delete Attendance
     *
     * @param int $attendance_id
     * @param int $section_id
     * @return JsonResponse
     */
    public function delete($attendance_id, $section_id)
    {
        try {
            (new AttendanceService())->setSectionId($section_id)
                                    ->deleteAttendance($attendance_id);

            $response['message'] = 'Attendance Deleted Successfully';
        } catch (NotFoundResourceException $e) {
            Log::error("Could not delete attendance (delete) : " . $e->getMessage());
            $response['error'] = $e->getMessage();
            $this->statusCode= 404;
        } catch (Exception $e) {
            Log::error("Could not delete attendance (delete) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode= 500;
        }

        return response()->json($response, $this->statusCode);
    }
}
