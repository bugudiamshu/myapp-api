<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\UserRepository;
use JWTAuth;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * App\Repositories\UserRepository.
     *
     * @var
     */
    protected $user_repo;

    /**
     * Create a new controller instance.
     */
    public function __construct(UserRepository $user_repo)
    {
        $this->middleware('guest')->except('logout');
        $this->user_repo = $user_repo;
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $credentials['active'] = 1;
        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid credentials'], 422);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = JWTAuth::setToken($token)->toUser();
        if (!$user->active) {
            return response()->json(['error' => 'valid credentials, user not active'], 422);
        }
        $user_details = $this->user_repo->getData($user->id);
        $user_details['roles'] = $user->roles->pluck('name');
        $user_details['org_type'] = $user->branch->organisation->organisation_type;
        $user_details['branch_header_text'] = $user->branch->organisation->name . ' - ' .$user->branch->name;
        // if no errors are encountered we can return a JWT

        return response()->json(compact('token', 'user_details'));
    }

    public function logout()
    {
        if (Auth::check()) {
            try {
                $token = JWTAuth::fromUser(Auth::user());
                JWTAuth::invalidate($token);
            } catch (\Exception $e) {
            }

            Auth::logout();
        }

        return response()->json([
            'message' => 'Successfully Logged out',
        ], 200);
    }
}
