<?php

namespace App\Http\Controllers;

use Auth;

class BaseController extends Controller
{
    public function canView($model)
    {
        if (!Auth::user()->can('view ' . $model)) {
            return false;
        }

        return true;
    }

    public function canCreate($model)
    {
        if (!Auth::user()->can('create ' . $model)) {
            return false;
        }

        return true;
    }

    public function canUpdate($model)
    {
        if (!Auth::user()->can('update ' . $model)) {
            return false;
        }

        return true;
    }

    public function canDelete($model)
    {
        if (!Auth::user()->can('delete ' . $model)) {
            return false;
        }

        return true;
    }

    public function unauthorizedResponse()
    {
        return response()->json([
            'message' => 'Not authorized',
        ], 401);
    }
}
