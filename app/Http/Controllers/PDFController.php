<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\Student;
use App\Models\User;
use App\Services\PrintService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PDFController extends Controller
{
    /**
     * Generate Section Wise Fee Detail PDF
     *
     * @param int $user_id
     * @param int $section_id
     * @param int $fee_type_id
     * @return void
     */
    public function generateSectionWiseFeePDF($user_id, $section_id, $fee_type_id)
    {
        try {
            $user = User::find($user_id);
            $print_service = new PrintService();
            $header_html = $print_service->getHeaderHtml($user);
            $print_service->setFeeTypeId($fee_type_id)
                ->setCourseDetail('section', $section_id)
                ->setBladeParameters('getSectionWiseFee', $section_id)
                ->getHtml('pdf.fee_details.section_wise_fee')
                ->outputPDF($header_html, 'sectionWiseReport.pdf');
        } catch (Exception $e) {
            Log::error('Couldn\'t print section wise fee (printSectionWiseFee): ' . $e->getMessage());
        }
    }

    /**
     * Generate Class Wise Fee Detail PDF
     *
     * @param int $user_id
     * @param int $class_id
     * @param int $fee_type_id
     * @return void
     */
    public function generateClassWiseFeePDF($user_id, $class_id, $fee_type_id)
    {
        try {
            $user = User::find($user_id);
            $print_service = new PrintService();
            $header_html = $print_service->getHeaderHtml($user);
            $print_service->setFeeTypeId($fee_type_id)
                ->setCourseDetail('class', $class_id)
                ->setBladeParameters('getClassWiseFee', $class_id)
                ->getHtml('pdf.fee_details.class_wise_fee')
                ->outputPDF($header_html, 'classWiseReport.pdf');
        } catch (Exception $e) {
            Log::error('Couldn\'t print class wise fee (printClassWiseFee): ' . $e->getMessage());
        }
    }

    /**
     * Generate Course Wise Fee Detail PDF
     *
     * @param int $user_id
     * @param int $course_id
     * @param int $fee_type_id
     * @return void
     */
    public function generateCourseWiseFeePDF($user_id, $course_id, $fee_type_id)
    {
        try {
            $user = User::find($user_id);
            $print_service = new PrintService();
            $header_html = $print_service->getHeaderHtml($user);
            $print_service->setFeeTypeId($fee_type_id)
                ->setCourseDetail('course', $course_id)
                ->setBladeParameters('getCourseWiseFee', $course_id)
                ->getHtml('pdf.fee_details.course_wise_fee')
                ->outputPDF($header_html, 'courseWiseReport.pdf');
        } catch (Exception $e) {
            Log::error('Couldn\'t print course wise fee (printCourseWiseFee): ' . $e->getMessage());
        }
    }

    /**
     * Generate all payments PDF
     *
     * @param int $user_id
     * @param int $student_id
     * @param int $fee_type_id
     * @return void
     */
    public function generatePaymentsPDF($user_id, $student_id, $fee_type_id)
    {
        try {
            $user = User::find($user_id);
            $print_service = new PrintService();
            $header_html = $print_service->getHeaderHtml($user);
            config(['mpdf.orientation' => 'P']);
            $print_service->setFeeTypeId($fee_type_id)
                ->setCourseDetail('student', $student_id)
                ->setBladeParameters('getPayments', $student_id)
                ->getHtml('pdf.fee_details.payments_receipt')
                ->outputPDF($header_html, 'paymentsReceipt.pdf');
            config(['mpdf.orientation' => 'L']);
        } catch (Exception $e) {
            Log::error('Couldn\'t print payments (generatePaymentsPDF): ' . $e->getMessage());
        }
    }

    /**
     * Generate Transactions PDF
     *
     * @param int $user_id
     * @param string $from_date
     * @param string $to_date
     * @param string $type
     * @param string $tag
     * @return void
     */
    public function generateTransactionsPDF($user_id, $from_date, $to_date, $type, $tag)
    {
        try {
            $user = User::find($user_id);
            $print_service = new PrintService();
            $header_html = $print_service->getHeaderHtml($user);
            $date_range = [
                'from_date' => $from_date,
                'to_date'   => $to_date
            ];
            config(['mpdf.orientation' => 'P']);
            $print_service->setUser($user)
                ->setBladeParameters('filterTransactions', [$type, $date_range, $tag])
                ->getHtml('pdf.transactions')
                ->outputPDF($header_html, 'transactions.pdf');
            config(['mpdf.orientation' => 'L']);
        } catch (Exception $e) {
            Log::error('Couldn\'t print transactions (generateTransactionsPDF): ' . $e->getMessage() .
                ' ' . $e->getLine());
        }
    }

    /**
     * Generate Section Wise Students List
     *
     * @param int $user_id
     * @param string $section_ids
     * @param string $column_names
     * @return void
     */
    public function generateSectionWiseStudentsListPDF($user_id, $section_ids, $column_names)
    {
        try {
            $user = User::find($user_id);
            $print_service = new PrintService();
            $header_html = $print_service->getHeaderHtml($user);
            $section_ids = unserialize(urldecode($section_ids));
            $column_names = unserialize(urldecode($column_names));
            $print_service->setUser($user)
                ->setBladeParameters('sectionWiseStudents', [$section_ids, $column_names])
                ->getHtml('pdf.section_wise_students')
                ->outputPDF($header_html, 'section_wise_students.pdf');
        } catch (Exception $e) {
            Log::error('Couldn\'t print section wise students list (generateSectionWiseStudentsListPDF): '
                . $e->getMessage() . ' ' . $e->getLine());
        }
    }

    /**
     * Generate Bonafide
     *
     * @param User $user_id
     * @param int $student_id
     * @return void
     */
    public function generateBonafidePDF($user_id, $student_id)
    {
        try {
            $user = User::find($user_id);
            $print_service = new PrintService();
            config(['mpdf.margin_top' => '5']);
            $print_service->setUser($user)
                ->setBladeParameters('bonafide', $student_id)
                ->getHtml('pdf.bonafide')
                ->outputPDF(null, 'Bonafide.pdf');
            config(['mpdf.margin_top' => '35']);
        } catch (Exception $e) {
            Log::error('Couldn\'t print bonafide (generateBonafide): '
                . $e->getMessage() . ' ' . $e->getLine());
        }
    }
}
