<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\Student;
use App\Models\StudentDetail;
use App\Services\StudentService;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use function PHPSTORM_META\map;

class StudentController extends BaseController
{
    protected $model = 'students';

    private $statusCode = 200;

    /**
     * Get all Students
     *
     * @return Collection||[]
     */
    public function index()
    {
        $students = Student::with(['studentDetails', 'studentFee', 'payments'])->where('is_alumni', false)->get();

        return response()->json($students, $this->statusCode);
    }

    /**
     * Create Student
     *
     * @param CreateStudentRequest $request
     * @return JsonResponse
     */
    public function create(CreateStudentRequest $request)
    {
        try {
            $student = (new StudentService())->createStudent($request);
            $response['message'] = config('constants.MESSAGES.CREATED');
            $response['student_id'] = $student->id;
        } catch (Exception $e) {
            Log::error("Create Student (create) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Edit Student
     *
     * @param int $student_id
     * @param UpdateStudentRequest $request
     * @return JsonResponse
     */
    public function edit($student_id, UpdateStudentRequest $request)
    {
        try {
            $student = Student::findOrFail($student_id);
            (new StudentService())->updateStudent($student, $request);
            $response['message'] = config('constants.MESSAGES.UPDATED');
        } catch (ModelNotFoundException $e) {
            $response['error'] = config('constants.ERRORS.404');
            $this->statusCode = 404;
        } catch (Exception $e) {
            Log::error("Update Student (edit) :" . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Delete Student
     *
     * @param int $student_id
     * @return JsonResponse
     */
    public function delete($student_id)
    {
        try {
            $student = Student::findOrFail($student_id);
            (new StudentService())->deleteStudent($student);
            $response['message'] = config('constants.MESSAGES.DELETED');
        } catch (ModelNotFoundException $e) {
            $response['error'] = config('constants.ERRORS.404');
            $this->statusCode = 404;
        } catch (Exception $e) {
            Log::error("Delete Student (delete) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Students by Section ID
     *
     * @param int $section_id
     * @return JsonResponse
     */
    public function getStudentsBySectionId($section_id)
    {
        try {
            $response = Student::where('is_alumni', false)
                ->where('section_id', $section_id)->get();
        } catch (Exception $e) {
            Log::error("Get Student By Section ID (getStudentsBySectionId) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Students Basic Info by Section ID
     *
     * @param int $section_id
     * @return JsonResponse
     */
    public function getStudentsBasicInfoBySectionId($section_id)
    {
        try {
            $response = Student::where('is_alumni', false)
                ->where('section_id', $section_id)->get();
        } catch (Exception $e) {
            Log::error("Get Student By Section ID (getStudentsBasicInfoBySectionId) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Student by ID
     *
     * @param int $student_id
     * @return JsonResponse
     */
    public function show($student_id)       // Not using this
    {
        $student = Student::with(['studentDetails', 'studentFee', 'payments'])
            ->where('is_alumni', false)
            ->where('id', $student_id)->first();

        if (!$student) {
            return response()->json([
                'error' => 'Student Not found',
            ], 404);
        }

        $student->class_id = $student->section->class->id;


        return response()->json($student, 200);
    }

    /**
     * Search Student by Name
     *
     * @param string $student_name
     * @return Collection||[]
     */
    public function search($student_name)
    {
        try {
            if (!empty($student_name)) {
                $response = Student::where('branch_id', Auth::user()->branch->id)
                    ->where('is_alumni', false)
                    ->where('student_name', 'LIKE', '%' . $student_name . '%')->get();
            } else {
                return [];
            }
        } catch (Exception $e) {
            Log::error("Search Student (search) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }


        return response()->json($response, $this->statusCode);
    }

    /**
     * Upload Image
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request)
    {
        try {
            $student = Student::findOrFail($request->student_id);
            if ($request->hasFile('student_image')) {
                (new StudentService())->uploadImage($student, $request->file('student_image'));
            } else {
                (new StudentService())->uploadImage($student);
            }
            $response['message'] = config("constants.MESSAGES.UPLOAD_SUCCESS");
        } catch (ModelNotFoundException $e) {
            $response['error'] = config('constants.ERRORS.404');
            $this->statusCode = 404;
        } catch (Exception $e) {
            Log::error("Upload Image (upload) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Student Basic Info
     *
     * @param int $student_id
     * @return JsonResponse
     */
    public function getStudentBasicInfo($student_id)
    {
        try {
            $student = Student::findOrFail($student_id);
            $response = (new StudentService())->getBasicInfo($student);
        } catch (ModelNotFoundException $e) {
            $response['error'] = config('constants.ERRORS.404');
            $this->statusCode = 404;
        } catch (Exception $e) {
            Log::error("Student Basic Info (getStudentBasicInfo) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Student Details by ID
     *
     * @param int $student_id
     * @return JsonResponse
     */
    public function getStudentDetails($student_id)
    {
        try {
            $student = Student::findOrFail($student_id);
            $response = $student->studentDetails;
        } catch (ModelNotFoundException $e) {
            $response['error'] = config('constants.ERRORS.404');
            $this->statusCode = 404;
        } catch (Exception $e) {
            Log::error("Student Details (getStudentDetails) : " . $e->getMessage());
            $response['error'] = config('constants.ERRORS.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Save Joining Details
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function saveJoiningDetails(Request $request)
    {
        $student_id = $request->student_id;
        $admission_number = $request->admission_number;
        $date_of_admission = date('Y-m-d', strtotime($request->date_of_admission));

        try {
            StudentDetail::updateOrCreate(
                ['student_id' => $student_id],
                ['admission_number' => $admission_number, 'date_of_admission' => $date_of_admission]
            );
            $response['message'] = config('constants.MESSAGES.UPDATED');
        } catch (Exception $e) {
            Log::error("Save Joining Details (saveJoiningDetails) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Save Personal Details
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function savePersonalDetails(Request $request)
    {
        $student_id = $request->student_id;
        $personal_details = json_decode($request->getContent(), true);
        $personal_details['date_of_birth'] = date('Y-m-d', strtotime($request->date_of_birth));

        try {
            StudentDetail::updateOrCreate(
                ['student_id' => $student_id],
                $personal_details
            );
            $response['message'] = config('constants.MESSAGES.UPDATED');
        } catch (Exception $e) {
            Log::error("Save Personal Details (savePersonalDetails) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    public function saveOtherDetails(Request $request)
    {
        $student_id = $request->student_id;
        $other_details = json_decode($request->getContent(), true);
        unset($other_details['student_id']);

        try {
            StudentDetail::updateOrCreate(
                ['student_id' => $student_id],
                $other_details
            );
            $response['message'] = config('constants.MESSAGES.UPDATED');
        } catch (Exception $e) {
            Log::error("Save Other Details (saveOtherDetails) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    public function saveRemovalDetails(Request $request)
    {
        $student_id = $request->student_id;
        $removalDetails = json_decode($request->getContent(), true);
        $removalDetails['date_of_removal'] = date('Y-m-d', strtotime($request->date_of_removal));
        unset($removalDetails['student_id']);

        try {
            StudentDetail::updateOrCreate(
                ['student_id' => $student_id],
                $removalDetails
            );
            $response['message'] = config('constants.MESSAGES.UPDATED');
        } catch (Exception $e) {
            Log::error("Save Removal Details (saveRemovalDetails) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }

    /**
     * Get Fields from Student and Student Details tables
     *
     * @return JsonResponse
     */
    public function getFields()
    {
        $column_names = [
            [
                'id' => 'father_name',
                'name' => 'Father Name'
            ],
            [
                'id' => 'mobile_number',
                'name' => 'Mobile Number'
            ],
            [
                'id' => 'admission_number',
                'name' => 'Admission Number'
            ],
            [
                'id' => 'date_of_admission',
                'name' => 'Date of Admission'
            ],
            [
                'id' => 'mother_name',
                'name' => 'Mother Name'
            ],
            [
                'id' => 'aadhaar_number',
                'name' => 'Aadhaar Number'
            ],
            [
                'id' => 'gender',
                'name' => 'Gender'
            ],
            [
                'id' => 'caste',
                'name' => 'Caste'
            ],
            [
                'id' => 'date_of_birth',
                'name' => 'Date of Birth'
            ],
            [
                'id' => 'address',
                'name' => 'Address'
            ]
        ];

        return response()->json($column_names, 200);
    }
}
