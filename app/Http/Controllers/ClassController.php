<?php

namespace App\Http\Controllers;

use App\Models\Clas;
use App\Services\PromoteClassService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ClassController extends Controller
{
    /**
     * Get Class by Course ID
     *
     * @param int $course_id
     * @return Collection|[]
     */
    public function getClassesByCourseId($course_id)
    {
        try {
            $response = Clas::where('course_id', $course_id)->get();
            /* $response = Cache::remember(
                "all_classes_".$course_id,
                config('constants.CACHE_EXPIRE'),
                function () use ($course_id) {
                    return Clas::where('course_id', $course_id)->get();
                }
            ); */
        } catch (Exception $e) {
            Log::error("Get classes by Course Id (getClassesByCourseId) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
        }

        return response()->json($response, 200);
    }

    /**
     * Promote Class
     *
     * @return JsonResponse
     */
    public function promoteClass(Request $request)
    {
        try {
            $response = (new PromoteClassService($request->from_class_id, $request->to_class_id))->promoteClass();
        } catch (Exception $e) {
            Log::error("Promote Classes (promoteClass) : " . $e->getTraceAsString());
            $response['error'] = config('constants.ERRORS.500');
        }

        return response()->json([
            'message' => 'Students successfully promoted'
        ], 200);
    }
}
