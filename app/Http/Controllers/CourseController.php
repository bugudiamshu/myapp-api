<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Section;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CourseController extends Controller
{
    private $statusCode = 200;

    /**
     * Get all Courses
     *
     * @return Collection||[]
     */
    public function index()
    {
        try {
            $branch_id = Auth::user()->branch->id;
            $response = Course::where('branch_id', $branch_id)->get();
            /* $response = Cache::remember(
                "all_courses_".$branch_id,
                config('constants.CACHE_EXPIRE'),
                function () use ($branch_id) {
                    return Course::where('branch_id', $branch_id)->get();
                }
            ); */
        } catch (Exception $e) {
            Log::error("Get Courses (index) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
            $this->statusCode = 500;
        }


        return response()->json($response, 200);
    }

    public function getCourseIdBySection($section_id)
    {
        try {
            $section = Section::find($section_id);
            $course_id = $section->class->course_id;
            $response = [
                'course_id' => $course_id,
                'class_id' => $section->class_id
            ];
        } catch (Exception $e) {
            Log::error("Get Courses by Section (getCourseIdBySection) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
            $this->statusCode = 500;
        }


        return response()->json($response, $this->statusCode);
    }

    /**
     * Get all courses
     *
     * @return JsonResponse
     */
    public function getAllCourses()
    {
        try {
            $response = Course::where('branch_id', Auth::user()->branch_id)
                ->get()
                ->map(function ($course) {
                    return [
                        'id'        => $course->id,
                        'name'      => $course->name,
                        'classes'   => $course->classes->map(function ($class) {
                            return [
                                'id'        => $class->id,
                                'name'      => $class->name,
                                'sections'  => $class->sections->map(function ($section) {
                                    return [
                                        'id'    => $section->id,
                                        'name'  => $section->name
                                    ];
                                })
                            ];
                        })
                    ];
                });
        } catch (Exception $e) {
            Log::error("Get all Courses (getAllCourses) : " . $e->getMessage()) . ' ' . $e->getLine();
            $response['error'] = config('constants.ERROR.500');
            $this->statusCode = 500;
        }

        return response()->json($response, $this->statusCode);
    }
}
