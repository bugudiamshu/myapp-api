<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Services\SectionService;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SectionController extends Controller
{
    /**
     * Get Section by Class ID
     *
     * @param int $class_id
     * @return Collection||[]
     */
    public function getSectionsByClassId($class_id)
    {
        try {
            $response = (new SectionService())->getSectionsByClass($class_id);
        } catch (Exception $e) {
            Log::error("Get Sections by Class Id (getSectionsByClassId) : " . $e->getMessage());
            $response['error'] = config('constants.ERROR.500');
        }

        return response()->json($response, 200);
    }
}
