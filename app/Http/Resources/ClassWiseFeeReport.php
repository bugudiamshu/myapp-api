<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClassWiseFeeReport extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'course_id'             => $this->course_id,
            'name'                  => $this->name,
            'total_committed_fee'   => $this->getCommittedFee(),
            'total_paid_fee'        => $this->getPaidFee(),
            'total_pending_fee'     => $this->getCommittedFee() - $this->getPaidFee(),
            'fee_details'           => $this->fee_details
        ];
    }
}
