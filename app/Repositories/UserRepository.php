<?php

namespace App\Repositories;

use Auth;
use Config;
use App\Models\User;

class UserRepository extends BasicRepository
{
    private const ERR_USR_LOGIN = 'Logged in user is not same as the request user ID ';
    private const ERR_USR_NOT_FOUND = 'User not found';

    /**
     * Specify the Model class name for the BasicRepository.
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\User';
    }

    /**
     *  Check if User is already Registered.
     */
    public function checkUser($username)
    {
        $user = User::where('username', $username)->get();
        if (count($user)) {
            return $user;
        }

        return false;
    }

    /**
     * Save user details.
     */
    public function saveData($request)
    {
        $user = $this->storeUser($request);

        // $user->assignRole('User');

        return true;
    }


    /**
     * Update user Data.
     */
    public function updateData($request, $id)
    {
        if ($id != Auth::id()) {
            return response()->json(self::ERR_USR_LOGIN, 403);
        }
        $user = User::find($id);
        if ($user) {
            $user = $this->updateUser($request, $id);
            return $user;
        } else {
            return response()->json([
                'message' => __(self::ERR_USR_NOT_FOUND),
            ], 404);
        }
    }

    /**
     * get user Data.
     */
    public function getData($id)
    {
        if ($id != Auth::id()) {
            return response()->json(self::ERR_USR_LOGIN, 403);
        }
        $user = User::where('id', $id)->first();
        if ($user) {
            // $data = $user->only($this->profile_fields);
            return $user;
        } else {
            return response()->json([
                'message' => __(self::ERR_USR_NOT_FOUND),
            ], 404);
        }
    }

    /***
     * Store User details
     */
    public function storeUser($request, $id = null)
    {
        $user = User::firstOrNew(['id' => $id]);
        $user->first_name = format_name($request->first_name);
        $user->last_name = format_name($request->last_name);
        $user->email = $request->email;
        $user->save();

        return $user;
    }

    /***
     * Update User details
     */
    public function updateUser($request, $id)
    {
        $user = User::find($id);
        $user->first_name = isset($request->first_name) ?
            format_name($request->first_name) : format_name($user->first_name);
        $user->last_name = isset($request->last_name) ?
            format_name($request->last_name) : format_name($user->last_name);
        $user->save();

        return $user;
    }

    public function inactive($id)
    {
        if ($id != Auth::id()) {
            return response()->json(self::ERR_USR_LOGIN, 403);
        }
        $user = User::where('id', $id)->first();
        if ($user) {
            $user->active = 0;
            $user->save();

            return response()->json([
                'message' => __('User de-activated Successfully'),
            ], 200);
        } else {
            return response()->json([
                'message' => __(self::ERR_USR_NOT_FOUND),
            ], 404);
        }
    }

    /**
     * get users list.
     */
    public function getUsers()
    {
        $users = User::all();
        $data = array();
        if ($users->count()) {
            foreach ($users as $user) {
                $data[] = $user;
            }
            return $data;
        } else {
            return response()->json([
                'message' => __('No Content'),
            ], 204);
        }
    }
}
