<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Branch;
use App\Models\Clas;
use App\Models\ClassFee;
use App\Models\Course;
use App\Models\Section;
use App\Policies\BranchPolicy;
use Spatie\Permission\Models\Permission;
use App\Policies\PermissionPolicy;
use Eminiarts\NovaPermissions\Role;
use App\Policies\RolePolicy;
use App\Models\User;
use App\Policies\ClassFeePolicy;
use App\Policies\ClassPolicy;
use App\Policies\CoursePolicy;
use App\Policies\SectionPolicy;
use App\Policies\UserPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Branch::class => BranchPolicy::class,
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        User::class => UserPolicy::class,
        Course::class => CoursePolicy::class,
        Clas::class => ClassPolicy::class,
        Section::class => SectionPolicy::class,
        ClassFee::class => ClassFeePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
