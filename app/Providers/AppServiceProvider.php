<?php

namespace App\Providers;

use App\Models\Branch;
use App\Models\ClassFee;
use App\Models\ImportStudent;
use App\Models\Organisation;
use App\Models\Role;
use App\Observers\BranchObserver;
use App\Observers\ClassFeeObserver;
use App\Observers\ImportStudentObserver;
use App\Observers\OrganisationObserver;
use App\Observers\RoleObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        ClassFee::observe(ClassFeeObserver::class);
        ImportStudent::observe(ImportStudentObserver::class);
        Organisation::observe(OrganisationObserver::class);
        Branch::observe(BranchObserver::class);
    }
}
