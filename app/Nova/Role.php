<?php

namespace App\Nova;

use App\Enums\RoleEnum;
use Eminiarts\NovaPermissions\Nova\Role as NovaRole;
use Laravel\Nova\Nova;
use Laravel\Nova\Resource;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\MorphToMany;
use Eminiarts\NovaPermissions\Checkboxes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Fields\Select;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Role extends NovaRole
{
    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        $userResource = Nova::resourceForModel(getModelForGuard($this->guard_name));

        return [
            ID::make('Id', 'id')
                ->rules('required')
                ->hideFromIndex(),

            Select::make(__('Name'), 'name')
                    ->options($this->getRoles())
                    ->rules(['required', 'string', 'max:255']),

            Text::make(__('Users'), function () {
                return count($this->users);
            })->exceptOnForms(),
            MorphToMany::make($userResource::label(), 'users', $userResource)->searchable(),
        ];
    }

    public function getRoles()
    {
        if (request()->user()->isSuperAdmin()) {
            return RoleEnum::toArray();
        } elseif (request()->user()->isAdmin()) {
            return Arr::except(RoleEnum::toArray(), [ RoleEnum::admin()->value, RoleEnum::superadmin()->value ]);
        }
    }

    public function getModelsBasedOnRole()
    {
        if (Auth::user()->isAdmin()) {
            return config('manage_models.admin');
        }
    }

    public function getPermissionsBasedOnRole()
    {
        if (Auth::user()->isAdmin()) {
            return config('except_permissions.admin');
        }
    }

    public static function availableForNavigation(Request $request)
    {
        return false;
    }
}
