<?php

namespace App\Services;

use App\Models\Alumni;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;

class AlumniService
{
    /**
     * Create Alumni
     *
     * @param Student $student
     * @param string $batch
     * @param  \Throwable  $exception
     * @return Collection
     */
    public function createAlumni($student, $batch)
    {
        $alumni = Alumni::create([
            'student_id' => $student->id,
            'course_of_removal_id' => $student->section->class->course_id,
            'class_of_removal_id' => $student->section->class_id,
            'branch_id' => Auth::user()->branch_id,
            'batch' => $batch,
        ]);

        if (!$alumni) {
            throw new Exception("Could not create Alumni");
        }

        return $alumni;
    }

    /**
     * Deactivate Alumni
     *
     * @param Student $student
     * @param  \Throwable  $exception
     * @return boolean
     */
    public function deactivateAlumni($student)
    {
        try {
            $student->is_alumni = true;
            $student->update();
            $student->studentFee()->update(['is_alumni' => 1]);
            $user = User::where('userable_type', \App\Models\Student::class)
                ->where('userable_id', $student->id)->first();
            $user->active = false;
            $user->update();
        } catch (Exception $e) {
            throw new Exception("Could not deactivate Alumni : " . $e->getMessage());
        }

        return true;
    }

    /**
     * Get Courses
     *
     * @param array $alumni
     * @return array
     */
    public function getCourses($alumni)
    {
        $courses = [];
        foreach ($alumni as $alumnus) {
            $course['course_id'] = $alumnus['course_id'];
            $course['course_name'] = $alumnus['course_name'];
            array_push($courses, $course);
        }
        return array_values(array_unique($courses, SORT_REGULAR));
    }

    /**
     * Get Classes
     *
     * @param array $alumni
     * @return array
     */
    public function getClasses($alumni)
    {
        $classes = [];
        foreach ($alumni as $alumnus) {
            $class['class_id'] = $alumnus['class_id'];
            $class['class_name'] = $alumnus['class_name'];
            array_push($classes, $class);
        }
        return array_values(array_unique($classes, SORT_REGULAR));
    }

    /**
     * Get Alumni
     *
     * @param string $batch
     * @param int $class_id
     * @return array
     */
    public function getAlumni($batch, $class_id)
    {
        $alumniList = [];
        $alumni = Alumni::where('batch', $batch)
            ->where('class_of_removal_id', $class_id)
            ->get();

        foreach ($alumni as $alumnus) {
            $student = $alumnus->student;
            $student_details = $student->studentDetails;
            array_push($alumniList, $student);
        }

        return $alumniList;
    }
}
