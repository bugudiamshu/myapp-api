<?php

namespace App\Services;

use App\Jobs\SendSMS;
use App\Models\SmsTemplate;
use App\Models\StudentPayment;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SMSService
{
    /**
     * Dispatch SMS
     *
     * @param string $template
     * @param array $mobile_numbers
     * @return void
     */
    public static function sendSMS($template, $mobile_numbers)
    {
        SendSMS::dispatch($template, $mobile_numbers);
    }

    /**
     * Send Payment Acknowledgement SMS
     *
     * @param StudentPayment $payment
     * @return void
     */
    public static function sendPaymentAcknowledgement($payment)
    {
        if ($sms_template = SmsTemplate::firstWhere('name', 'PAYMENT_ACKNOWLEDGEMENT')) {
            $template = sprintf(
                $sms_template->template,
                $payment->paid_amount,
                strtoupper($payment->student->student_name),
                $payment->receipt_number,
                Carbon::parse($payment->receipt_date)->format('d/m/Y'),
                $payment->student->total_pending_fee
            );
            logger($template);
            self::sendSMS($template, [
                $payment->student->mobile_number
            ]);
        }
    }

    /**
     * Send Daily Transaction Summary
     *
     * @param User $user
     * @param string $date
     * @param int $income
     * @param int $expense
     * @return void
     */
    public static function sendDailyTransactionSummary($user, $date, $income, $expense)
    {
        if ($sms_template = $user->smsTemplates()->firstWhere('name', 'DAILY_TRANSACTION_SUMMARY')) {
            $template = sprintf(
                $sms_template->template,
                $user->first_name . ' ' . $user->last_name,
                $date,
                $income,
                $expense,
                $user->branch->name
            );

            self::sendSMS($template, [
                $user->mobile_number
            ]);
        }
    }

    /**
     * Send Income SMS
     *
     * @param User $user
     * @param Transaction $transaction
     * @return void
     */
    public static function sendIncomeSMS($user, $transaction)
    {
        $user->branch->onlyAdmins()
            ->map(function ($user) use ($transaction) {
                if (
                    $sms_template = $user->smsTemplates()
                    ->where('name', 'INDIVIDUAL_INCOME_TRANSACTION')
                    ->first()
                ) {
                    $template = sprintf(
                        $sms_template->template,
                        $user->first_name . ' ' . $user->last_name,
                        $transaction->amount,
                        $transaction->purpose,
                        $user->branch->name,
                    );

                    self::sendSMS($template, [
                        $user->mobile_number
                    ]);
                }
            });
    }

    /**
     * Send Expenditure SMS
     *
     * @param User $user
     * @param Transaction $transaction
     * @return void
     */
    public static function sendExpenditureSMS($user, $transaction)
    {
        $user->branch->onlyAdmins()
            ->map(function ($user) use ($transaction) {
                if (
                    $sms_template = $user->smsTemplates()
                    ->where('name', 'INDIVIDUAL_EXPENDITURE_TRANSACTION')
                    ->first()
                ) {
                    $template = sprintf(
                        $sms_template->template,
                        $user->first_name . ' ' . $user->last_name,
                        $transaction->amount,
                        $transaction->purpose,
                        $user->branch->name,
                    );

                    self::sendSMS($template, [
                        $user->mobile_number
                    ]);
                }
            });
    }

    /**
     * Send Birthday SMS
     *
     * @param string $student_name
     * @param string $mobile_number
     * @return void
     */
    public static function sendBirthdaySMS($student_name, $mobile_number)
    {
        if ($sms_template = SmsTemplate::firstWhere('name', 'BIRTHDAY_V2')) {
            $template = sprintf(
                $sms_template->template,
                $student_name
            );

            self::sendSMS($template, [
                $mobile_number
            ]);
        }
    }

    public static function sendDailyAttendanceSMS($studentName, $session, $mobileNumber)
    {
        if ($smsTemplate = SmsTemplate::firstWhere('name', 'DAILY_ATTENDANCE_V2')) {
            $template = sprintf(
                $smsTemplate->template,
                $studentName,
                $session
            );

            self::sendSMS($template, [
                $mobileNumber
            ]);
        }
    }

    /**
     * Send Fee Reminder SMS
     *
     * @param string $studentName
     * @param string $mobileNumber
     * @param string $pendingFee
     * @return void
     */
    public static function sendFeeReminderSMS($studentName, $mobileNumber, $pendingFee)
    {
        if (($smsTemplate = SmsTemplate::firstWhere('name', 'FEE_REMINDER')) && $pendingFee !== 0) {
            $template = sprintf(
                $smsTemplate->template,
                strtoupper($studentName),
                $pendingFee
            );

            Log::info($template);

            self::sendSMS($template, [
                $mobileNumber
            ]);
        }
    }
}
