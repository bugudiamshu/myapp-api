<?php

namespace App\Services;

use App\Models\ClassFee;
use App\Models\FeeType;
use App\Models\Section;
use App\Models\Student;
use App\Models\StudentFee;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PromoteClassService
{
    private $fromClassId;

    private $toClassId;


    public function __construct($from_class_id, $to_class_id)
    {
        $this->fromClassId = $from_class_id;
        $this->toClassId = $to_class_id;
    }
    
    /**
     * Promote Classes
     *
     * @return void
     */
    public function promoteClass()
    {
        if ($this->validatePromoteClasses()) {
            $from_class_sections = (new SectionService())->getSectionsByClass($this->fromClassId)->toArray();
            $to_class_sections = (new SectionService())->getSectionsByClass($this->toClassId)->toArray();
            $this->createPromotedSections($from_class_sections, $to_class_sections);
            $this->removeUnwantedSections($from_class_sections, $to_class_sections);
            $this->promoteStudents($from_class_sections, $to_class_sections);
        }
    }

    /**
     * Validate Promote classes
     *
     * @return boolean
     */
    public function validatePromoteClasses()
    {
        if ($this->fromClassId == $this->toClassId) {
            throw new Exception("From and To classes should not be same");
        }

        return true;
    }

    /**
     * Remove unwanted sections from the destination class
     *
     * @param array $from_class_sections
     * @param array $to_class_sections
     * @return void
     */
    public function removeUnwantedSections($from_class_sections, $to_class_sections)
    {
        $unnecessary_sections = array_diff(
            array_column($to_class_sections, 'name'),
            array_column($from_class_sections, 'name')
        );

        foreach ($unnecessary_sections as $section) {
            $section = Section::where('name', $section)
                ->where('class_id', $this->toClassId);
            if (!$section->delete()) {
                throw new Exception("Could not delete unnecessary section " . $section->id);
            }
        }
    }

    /**
     * Create sections in the destination class
     *
     * @param array $from_class_sections
     * @param array $to_class_sections
     * @return void
     */
    public function createPromotedSections($from_class_sections, $to_class_sections)
    {
        $new_sections = array_diff(
            array_column($from_class_sections, 'name'),
            array_column($to_class_sections, 'name'),
        );

        try {
            foreach ($new_sections as $section) {
                Section::firstOrCreate(
                    [
                        'name' => $section,
                        'class_id' => $this->toClassId
                    ]
                );
            }
        } catch (Exception $e) {
            Log::error("Could not promote sections " . $e->getMessage());
        }
    }

    /**
     * Promote Students
     *
     * @param array $from_class_sections
     * @param array $to_class_sections
     * @return void
     */
    public function promoteStudents($from_class_sections, $to_class_sections)
    {
        array_map(
            function ($from_section) use ($to_class_sections) {
                $to_section = array_filter($to_class_sections, function ($to_class_section) use ($from_section) {
                    return strtolower($from_section['name']) == strtolower($to_class_section['name']);
                });

                if (isset($to_section[0])) {
                    $section = $to_section[0];
                    if ($this->resetPaymentData($from_section['id'], $this->toClassId)) {
                        Student::where('section_id', $from_section['id'])
                            ->where('branch_id', Auth::user()->branch_id)
                            ->update([
                                'section_id' => $section['id']
                            ]);
                    }
                }

                return;
            },
            $from_class_sections
        );
    }

    /**
     * Reset Payment Data
     *
     * @param int $section_id
     * @param int $class_id
     * @return void
     */
    public function resetPaymentData($section_id, $class_id)
    {
        try {
            $students = Section::findOrFail($section_id)->students;
            foreach ($students as $student) {
                $previous_balance = $student->total_student_fee - $student->total_paid_fee;
                $student->payments()->delete();
                $student->studentFee()->delete();
                if ($previous_balance != 0) {
                    $student->section_id = $section_id;
                    $this->addPreviousBalance($student, $previous_balance);
                }
                $this->createStudentFee($student, $class_id);
            }
        } catch (Exception $e) {
            Log::error("Reset Payment Data : " . $e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Add Previous Balance
     *
     * @param Student $student
     * @param int $previous_balance
     * @return void
     */
    public function addPreviousBalance($student, $previous_balance)
    {
        try {
            $fee_type_id = FeeType::findOrCreate('Previous Balance')->id;
            StudentFee::updateOrCreate(
                [
                    'student_id' => $student->id,
                    'fee_type_id' => $fee_type_id
                ],
                [
                    'section_id' => $student->section_id,
                    'class_id' => $student->section->class_id,
                    'course_id' => $student->section->class->course_id,
                    'amount' => $previous_balance
                ]
            );
        } catch (Exception $e) {
            Log::error("Add Previous Balance : " . $e->getMessage());
        }
        
    }

    /**
     * Create Student Fee with the new Class Fee
     *
     * @param Student $student
     * @param int $class_id
     * @return void
     */
    public function createStudentFee($student, $class_id)
    {
        try {
            $class_fees = ClassFee::where('class_id', $class_id)->get();
            foreach ($class_fees as $class_fee) {
                StudentFee::create([
                    'student_id' => $student->id,
                    'fee_type_id' => $class_fee->fee_type_id,
                    'section_id' => $student->section->id,
                    'class_id' => $student->section->class->id,
                    'course_id' => $student->section->class->course->id,
                    'amount' => $class_fee->amount,
                    'is_alumni' => isset($student->is_alumni) ? $student->is_alumni : false,
                ]);
            }
        } catch (Exception $e) {
            Log::error("Create Student Fee : " . $e->getMessage());
        }
    }
}
