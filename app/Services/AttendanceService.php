<?php

namespace App\Services;

use App\Exceptions\NotCreatedException;
use App\Models\StudentAttendance;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class AttendanceService
{

    private $absentIds;

    private $sessionId;

    private $date;

    private $sectionId;

    /**
     * Get the value of absentIds
     */
    public function getAbsentIds()
    {
        return $this->absentIds;
    }

    /**
     * Set the value of absentIds
     *
     * @return  self
     */
    public function setAbsentIds($absentIds)
    {
        $this->absentIds = $absentIds;

        return $this;
    }

    /**
     * Get the value of sessionId
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * Set the value of sessionId
     *
     * @return  self
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get the value of date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of sectionId
     */
    public function getSectionId()
    {
        return $this->sectionId;
    }

    /**
     * Set the value of sectionId
     *
     * @return  self
     */
    public function setSectionId($sectionId)
    {
        $this->sectionId = $sectionId;

        return $this;
    }

    /**
     * Create Attendance
     *
     * @return StudentAttendance
     */
    public function createAttendance()
    {
        $attendance = StudentAttendance::firstOrCreate(
            [
                'session_id' => $this->getSessionId(),
                'date' => $this->getDate(),
            ],
            [
                'absent_json' => json_encode([
                    $this->newSection()
                ])
            ]
        );

        if (!$attendance) {
            throw new Exception("Unable to mark attendance for this session");
        }

        return $attendance;
    }


    /**
     * Create an array of a new Section Absentees
     *
     * @return array
     */
    private function newSection()
    {
        return [
            'section_id' => $this->getSectionId(),
            'absent_ids' => $this->getAbsentIds()
        ];
    }

    /**
     * Update Attendance for the existing session id and date
     *
     * @param StudentAttendance $student_attendance
     * @return void
     */
    public function updateAttendance($student_attendance)
    {
        $absent_sections = json_decode($student_attendance->absent_json, true);
        $section_exist = false;
        foreach ($absent_sections as $key => $absent_section) {
            if ($absent_section['section_id'] == $this->getSectionId()) {
                $absent_sections[$key]['absent_ids'] = $this->getAbsentIds();
                $section_exist = true;
            }
        }

        if (!$section_exist) {
            array_push($absent_sections, $this->newSection());
        }
        $student_attendance->absent_json = json_encode($absent_sections);
        $student_attendance->update();
    }

    /**
     * Get Attendance
     *
     * @return array
     */
    public function getAttendance()
    {
        $data = [];
        if ($student_attendance = StudentAttendance::where('session_id', $this->sessionId)
            ->where('date', $this->date)
            ->first()) {
            $data = $this->getSectionData($student_attendance);
            if ($data) {
                $data = array_values($data)[0];
                $data['id'] = $student_attendance->id;
            }
        }
        return $data;
    }

    /**
     * Delete Attendance
     *
     * @param int $attendance_id
     * @return void
     */
    public function deleteAttendance($attendance_id)
    {
        $student_attendance = StudentAttendance::find($attendance_id);
        $absent_data = $this->getSectionData($student_attendance);
        if ($absent_data) {
            $section_offset = array_keys($absent_data)[0];
            $absent_array = json_decode($student_attendance->absent_json, true);
            if (count($absent_array) == 1 && $student_attendance->delete()) {
                return;
            }
            unset($absent_array[$section_offset]);
            $student_attendance->update([
                'absent_json' => json_encode($absent_array)
            ]);
        } else {
            throw new NotFoundResourceException("Section not found");
        }
        
    }

    /**
     * Get Absent Data
     *
     * @param StudentAttendance $student_attendance
     * @return array
     */
    public function getSectionData($student_attendance)
    {
        $data = null;
        $absent_array = json_decode($student_attendance->absent_json, true);
        $section_data = array_filter($absent_array, function ($absent) {
            return $absent['section_id'] == $this->sectionId;
        });

        if (count($section_data) > 0) {
            $data = $section_data;
        }

        return $data;
    }
}
