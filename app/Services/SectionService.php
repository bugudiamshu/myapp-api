<?php

namespace App\Services;

use App\Models\Section;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SectionService
{
    /**
     * Get Sections by Class
     *
     * @param int $class_id
     * @return Collection
     */
    public function getSectionsByClass($class_id)
    {
        return Section::where('class_id', $class_id)->get();
        $sections = Cache::remember(
            "all_sections_".$class_id,
            config('constants.CACHE_EXPIRE'),
            function () use ($class_id) {
                return Section::where('class_id', $class_id)->get();
            }
        );

        return $sections;
    }
}