<?php

namespace App\Services;

use App\Exceptions\MethodNotFoundException;
use App\Http\Requests\CreateStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\Student;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class StudentService
{
    public function __call($name, $arguments)
    {
        if (!method_exists($this, $name)) {
            throw new MethodNotFoundException($name . " method doesn't exist");
        }
    }

    /**
     * Create Student
     *
     * @param CreateStudentRequest $request
     * @return Student
     */
    public function createStudent(CreateStudentRequest $request)
    {
        $student = Student::create([
            'branch_id' => Auth::user()->branch_id,
            'section_id' => $request->section_id,
            'student_name' => $request->student_name,
            'father_name' => $request->father_name,
            'mobile_number' => $request->mobile_number,
        ]);
            
        if (!$student) {
            throw new Exception("Couldn't create Student");
        }
        
        return $student;
    }

    /**
     * Update Student
     *
     * @param Student $student
     * @param UpdateStudentRequest $request
     * @return void
     */
    public function updateStudent($student, UpdateStudentRequest $request)
    {
        $student->student_name = $request->student_name;
        $student->father_name = $request->father_name;
        $student->mobile_number = $request->mobile_number;
        if (empty($request->student_image)) {
            $student->image = 'student_dummy.png';
        }
        
        if (!$student->update()) {
            throw new Exception("Couldn't update Student");
        }
    }

    /**
     * Delete Student
     *
     * @param Student $student
     * @return void
     */
    public function deleteStudent($student)
    {
        if (!$student->delete()) {
            throw new Exception("Couldn't delete Student");
        }
    }

    /**
     * Upload Student Image
     *
     * @param Student $student
     * @param UploadedFile $student_image
     * @return void
     */
    public function uploadImage($student, $student_image = null)
    {
        if ($student_image) {
            $student_image = $student_image;
            $name = $student->id . '_' . date('YmdHis') . '.' . $student_image->getClientOriginalExtension();
            $stored_image = $student_image->storePubliclyAs(
                '/',
                $name,
                'student_images'
            );
            $student->image = $stored_image; // Storage::disk('student_images')->url('images/students/' . $path);
        } else {
            $student->image = 'student_dummy.png';
        }
        
        if (!$student->update()) {
            throw new Exception("Couldn't upload Image");
        }
    }

    /**
     * Get Student Basic Info
     *
     * @param Student $student
     * @return array
     */
    public function getBasicInfo($student)
    {
        return [
            'id'                => $student->id,
            'student_name'      => $student->student_name,
            'father_name'       => $student->father_name,
            'mobile_number'     => $student->mobile_number,
            'image'             => $student->image,
            'course_name'       => $student->section->class->course->name,
            'class_name'        => $student->section->class->name,
            'section_name'      => $student->section->name,
            'student_image'     => Storage::disk('student_images')->url('images/students/' . $student->image)
        ];
    }
}
