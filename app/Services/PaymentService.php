<?php

namespace App\Services;

use App\Exceptions\PaymentException;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Models\FeeType;
use App\Models\Student;
use App\Models\StudentPayment;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

class PaymentService
{

    public function createPayment($request)
    {
        $student = Student::findOrFail($request->student_id);
        $student_payment = StudentPayment::create([
            'student_id' => $student->id,
            'fee_type_id' => $request->fee_type_id,
            'section_id' => $student->section_id,
            'class_id' => $student->section->class_id,
            'course_id' => $student->section->class->course_id,
            'receipt_date' => date('Y-m-d', strtotime($request->receipt_date)),
            'receipt_number' => $request->receipt_number,
            'paid_amount' => $request->amount,
            'branch_id' => Auth::user()->branch_id,
            'paid_by' => $request->paid_by,
            'collected_by' => $request->collected_by,
            'is_alumni' => $student->is_alumni
        ]);

        if (!$student_payment) {
            throw new PaymentException("Could not create Payment");
        }

        return $student_payment;
    }

    /**
     * Validate payment when Creating
     *
     * @param int $student_id
     * @param int $fee_type_id
     * @param int $pay_amount
     * @param  \Throwable  $exception
     * @return boolean
     */
    public function validateCreatePayment($student_id, $fee_type_id, $pay_amount)
    {
        $student = Student::findOrFail($student_id);
        $paid_amount = $student->getTotalPaidFeeAttribute($fee_type_id);
        $committed_amount = $student->getTotalStudentFeeAttribute($fee_type_id);
        $fee_name = FeeType::find($fee_type_id)->name;

        if ($pay_amount > $committed_amount) {
            throw new PaymentException("Paying amount exceeding the " . $fee_name);
        } elseif (($paid_amount + $pay_amount) > $committed_amount) {
            throw new PaymentException("Total payment exceeding the " . $fee_name);
        }

        return true;
    }

    /**
     * Update payment
     *
     * @param StudentPayment $payment
     * @param UpdatePaymentRequest $request
     * @param  \Throwable  $exception
     * @return void
     */
    public function updatePayment($payment, $request)
    {
        try {
            $payment->update([
                'fee_type_id' => $request->fee_type_id,
                'receipt_date' => date('Y-m-d', strtotime($request->receipt_date)),
                'branch_id' => Auth::user()->branch_id,
                'receipt_number' => $request->receipt_number,
                'paid_amount' => $request->amount,
                'paid_by' => $request->paid_by,
                'collected_by' => $request->collected_by,
            ]);
        } catch (Exception $e) {
            throw new Exception("Cannot update update payment (updatePayment) : " . $e->getMessage());
        }
    }

    /**
     * Validate payment when Updating
     *
     * @param StudentPayment $payment
     * @param int $fee_type_id
     * @param int $paying_amount
     * @param  \Throwable  $exception
     * @return boolean
     */
    public function validateUpdatePayment($payment, $fee_type_id, $paying_amount)
    {
        $student = $payment->student;

        $total_paid_amount = $student->getTotalPaidFeeAttribute($fee_type_id);
        $total_committed_amount = $student->getTotalStudentFeeAttribute($fee_type_id);
        $amount_paid = $payment->paid_amount;
        $fee_name = $payment->feeType->name;

        if ($paying_amount > $total_committed_amount) {
            throw new PaymentException("Paying amount exceeding the " . $fee_name);
        } elseif (($total_paid_amount - $amount_paid + $paying_amount) > $total_committed_amount) {
            throw new PaymentException("Total payment exceeding the " . $fee_name);
        }

        return true;
    }

    public function previousPayments($student_id, $fee_type_id = null)
    {
        $student = Student::findOrFail($student_id);
        $student_fee = $student->studentFee;
        $student_payments = $student->payments->each(function ($payment) {
            $payment->iso_receipt_date = Carbon::createFromFormat('Y-m-d', $payment->receipt_date)->toIso8601String();
            $payment->receipt_date = Carbon::parse($payment->receipt_date)->format('d-m-Y');
            return $payment;
        });

        if ($fee_type_id) {
            $student_fee = $student_fee->where('fee_type_id', $fee_type_id);
            $student_payments = $student_payments->where('fee_type_id', $fee_type_id);
            $fee_name = $student_fee->first()->feeType->name;
        }

        $committed_fee = $student_fee->sum('amount');
        $paid_amount = $student_payments->sum('paid_amount');

        return [
            'fee_name' => isset($fee_type_id) ? $fee_name : 'Total Committed Fee',
            'payments' => array_values($student_payments->toArray()),
            'committed_fee' => $committed_fee,
            'paid_total' => $paid_amount,
            'pending_fee' => $committed_fee - $paid_amount
        ];
    }
}
