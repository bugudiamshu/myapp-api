<?php

namespace App\Services;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FileUploadService
{
    /**
     * Upload Files
     *
     * @param Model $model
     * @param File $file
     * @param string $disk
     * @return Model
     */
    public function upload($model, $file, $disk)
    {
        try {
            $name = $model->id . '_' . date('YmdHis') . '.' . $file->getClientOriginalExtension();
            $stored_image = $file->storePubliclyAs(
                '/',
                $name,
                $disk
            );
            $model->file = $stored_image;
            $model->save();
        } catch (Exception $e) {
            Log::error("Upload File (fileUpload) for " . $model . " failed : " . $e->getMessage()
                . ' ' . $e->getLine());
            $model->delete();
            return;
        }

        return $model;
    }
}
