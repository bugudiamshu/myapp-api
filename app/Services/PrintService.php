<?php

namespace App\Services;

use App\Models\Clas;
use App\Models\Course;
use App\Models\Section;
use App\Models\Student;
use App\Models\StudentPayment;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Mpdf\Mpdf;

class PrintService
{
    private $feeTypeId;

    private $bladeParameters;

    public $html;

    private $courseDetail;

    /**
     * Get the value of feeTypeId
     */
    public function getFeeTypeId()
    {
        return $this->feeTypeId;
    }

    /**
     * Set User
     *
     * @return  self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set the value of feeTypeId
     *
     * @return  self
     */
    public function setFeeTypeId($feeTypeId)
    {
        $this->feeTypeId = $feeTypeId == 'total_fee' ? null : $feeTypeId;

        return $this;
    }

    /**
     * Set the value of blade parameters
     *
     * @return  self
     */
    public function setBladeParameters($fn_name, $arguments)
    {
        if (is_array($arguments)) {
            $this->bladeParameters = call_user_func_array(array($this, $fn_name), $arguments);
        } else {
            $this->bladeParameters = $this->$fn_name($arguments);
        }

        return $this;
    }

    /**
     * Get the value of html
     */
    public function getHtml($blade)
    {
        $html = View::make($blade)->with($this->bladeParameters);
        $this->html = $html->render();

        return $this;
    }

    /**
     * Set the value of html
     *
     * @return  self
     */
    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    /**
     * Set Course Detail data
     *
     * @param string $entity
     * @param int $entity_id
     * @return self
     */
    public function setCourseDetail($entity, $entity_id)
    {
        $course_detail = [];
        switch ($entity) {
            case 'section':
                $section = Section::find($entity_id);
                $course_detail = [
                    'section_name'  => $section->name,
                    'class_name'    => $section->class->name,
                    'course_name'   => $section->class->course->name
                ];
                break;
            case 'class':
                $class = Clas::find($entity_id);
                $course_detail = [
                    'class_name'    => $class->name,
                    'course_name'   => $class->course->name
                ];
                break;
            case 'course':
                $course = Course::find($entity_id);
                $course_detail = [
                    'course_name'   => $course->name
                ];
                break;
            case 'student':
                $student = Student::find($entity_id);
                $course_detail = [
                    'student_name'  => $student->student_name,
                    'section_name'  => $student->section->name,
                    'class_name'    => $student->section->class->name
                ];
                break;
        }

        $this->courseDetail = $course_detail;

        return $this;
    }

    /**
     * Get Header Info Data
     *
     * @param User $user
     * @return void
     */
    public function getHeaderInfo($user)
    {
        return [
            'organisation_name' => $user->branch->organisation->name,
            'branch_name'       => $user->branch->name
        ];
    }

    /**
     * Get Header HTML as string
     *
     * @param User $user
     * @return string
     */
    public function getHeaderHTML($user)
    {
        return $this->setBladeParameters('getHeaderInfo', $user)
            ->getHtml('pdf.fee_details.header')
            ->html;
    }

    /**
     * Output the PDF
     *
     * @param string $header_html
     * @param string $filename
     * @return void
     */
    public function outputPDF($header_html, $filename)
    {
        $mpdf = new Mpdf(config('mpdf'));
        $mpdf->SetHTMLHeader($header_html);
        $mpdf->setTitle(explode('.', $filename)[0]);
        if ($header_html) {
            $mpdf->setFooter('Report as on ' . Carbon::now()->format('M d, Y h:i:s A') . '||{PAGENO}');
        }
        $mpdf->WriteHTML(file_get_contents(url('/css/mpdf.css')), 1);
        $mpdf->WriteHTML($this->html);

        $mpdf->Output($filename, 'I');
    }

    /**
     * Get Section Wise Fee Data
     *
     * @param int $section_id
     * @return array
     */
    public function getSectionWiseFee($section_id)
    {
        $students = Student::where('section_id', $section_id)
            ->without(['studentDetails'])
            ->get()->map(function ($student) {
                $student->setAppends([]);
                $student['committed_fee'] = $student->getCommittedFee($this->feeTypeId);
                $student['paid_fee'] = $student->getPaidFee($this->feeTypeId);
                return $student;
            });

        return [
            'students'  => $students,
            'total_committed_fee' => $students->sum('committed_fee'),
            'total_paid_fee' => $students->sum('paid_fee'),
            'course_detail' => $this->courseDetail
        ];
    }

    /**
     * Get Class Wise Fee Data
     *
     * @param int $class_id
     * @return array
     */
    public function getClassWiseFee($class_id)
    {
        $sections = Section::where('class_id', $class_id)
            ->get()->map(function ($section) {
                $section->setAppends([]);
                $section['committed_fee'] = $section->getCommittedFee($this->feeTypeId);
                $section['paid_fee'] = $section->getPaidFee($this->feeTypeId);
                return $section;
            });

        return [
            'sections'  => $sections,
            'total_committed_fee' => $sections->sum('committed_fee'),
            'total_paid_fee' => $sections->sum('paid_fee'),
            'course_detail' => $this->courseDetail
        ];
    }

    /**
     * Get Course Wise Fee Data
     *
     * @param int $course_id
     * @return array
     */
    public function getCourseWiseFee($course_id)
    {
        $classes = Clas::where('course_id', $course_id)
            ->get()->map(function ($class) {
                $class->setAppends([]);
                $class['committed_fee'] = $class->getCommittedFee($this->feeTypeId);
                $class['paid_fee'] = $class->getPaidFee($this->feeTypeId);
                return $class;
            });

        return [
            'classes'  => $classes,
            'total_committed_fee' => $classes->sum('committed_fee'),
            'total_paid_fee' => $classes->sum('paid_fee'),
            'course_detail' => $this->courseDetail
        ];
    }

    /**
     * Get Payments
     *
     * @param int $student_id
     * @return array
     */
    public function getPayments($student_id)
    {
        if ($this->feeTypeId == 'undefined') {
            $this->feeTypeId = null;
        }

        return array_merge(
            (new PaymentService())->previousPayments($student_id, $this->feeTypeId),
            [
                'student_detail' => $this->courseDetail
            ]
        );
    }

    /**
     * Filter Transactions
     *
     * @param string $transaction_type
     * @param array $date_range
     * @param string $tag
     * @return array
     */
    public function filterTransactions($transaction_type, $date_range, $tag)
    {
        $payments = [];
        $transactions = Transaction::whereBetween('transaction_date', $date_range)
            ->forBranch($this->user->branch);
        if ($transaction_type != 'both') {
            $transactions = $transactions->where('transaction_type', $transaction_type);
        }

        if ($transaction_type != 'expense') {
            $payments = StudentPayment::whereBetween('receipt_date', $date_range)
                ->forBranch($this->user->branch)
                ->orderBy('receipt_number', 'ASC')
                ->get()
                ->map(function ($payment) {
                    return collect([
                        'id'                => null,
                        'transaction_type'  => 'income',
                        'amount'            => $payment->paid_amount,
                        'purpose'           => 'Receipt #' . $payment->receipt_number,
                        'transaction_date'  => $payment->receipt_date,
                        'tag'               => 'all'
                    ]);
                });
        }

        $transactions = collect($payments)->merge($transactions->get())->groupBy('transaction_date')->toArray();
        ksort($transactions);
        $transactions = collect($transactions)->flatten(1);

        if ($tag != 'all') {
            $transactions = $transactions->where('tag', $tag);
        }

        $unique_date = '';
        $new_transactions = [];
        foreach ($transactions as $transaction) {
            if ($unique_date == $transaction['transaction_date']) {
                $transaction['transaction_date'] = null;
            } else {
                $unique_date = $transaction['transaction_date'];
            }
            array_push($new_transactions, $transaction);
        }

        $new_transactions = collect($new_transactions);

        return [
            'from_date'     => isset($date_range['from_date']) ? $date_range['from_date'] : $date_range[0],
            'to_date'       => isset($date_range['to_date']) ? $date_range['to_date'] : $date_range[0],
            'total_income'  => $transactions->where('transaction_type', 'income')->sum('amount'),
            'total_expense' => $transactions->where('transaction_type', 'expense')->sum('amount'),
            'transactions'  => $new_transactions,
            'tag'           => $tag
        ];
    }

    /**
     * Section Wise Students
     *
     * @param array $section_ids
     * @param array $column_names
     * @return array
     */
    public function sectionWiseStudents($section_ids, $column_names)
    {
        $student_columns = ['student_name'];
        $index = array_search('father_name', $column_names);
        if (!$index) {
            array_push($student_columns, 'father_name');
            unset($column_names[$index]);
        }
        $index = array_search('mobile_number', $column_names);
        if ($index = array_search('mobile_number', $column_names)) {
            array_push($student_columns, 'mobile_number');
            unset($column_names[$index]);
        }

        $students = Student::whereIn('section_id', $section_ids)
            ->get()
            ->map(function ($student) use ($student_columns, $column_names) {
                $student_item = [];
                foreach ($student_columns as $student_column) {
                    $student_item[$student_column] = $student->$student_column;
                    $student_item['section'] = $student->section->name;
                }

                foreach ($column_names as $column_name) {
                    $student_item[$column_name] =  $student->studentDetails->$column_name;
                }

                return $student_item;
            });

        $section = Section::findOrFail($section_ids[0]);

        return [
            'students'      => $students,
            'column_names'  => array_merge($student_columns, $column_names),
            'course'        => $section->class->course->name,
            'class'         => $section->class->name,
            'sections'      => Section::whereIn('id', $section_ids)->get()->map(function ($section) {
                return $section->name;
            })->join(", ")
        ];
    }

    /**
     * Bonafide
     *
     * @param int $student_id
     * @return array
     */
    public function bonafide($student_id)
    {
        return [
            'student' => Student::findOrFail($student_id)
        ];
    }
}
