<?php

namespace App\Processors;

use App\Models\Clas;
use App\Models\Course;
use App\Models\ImportStudent;
use App\Models\Section;
use App\Models\Student;
use App\Models\StudentDetail;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class BulkStudentImportProcessor implements ToCollection, WithHeadingRow
{
    use Importable;

    private $import_student;

    public function __construct(ImportStudent $import_student)
    {
        $this->import_student = $import_student;
    }

    public function collection(Collection $rows)
    {
        $request_data = [];
        $response_data = [];
        foreach ($rows as $row) {
            if ($row['student_name'] || !empty($row['student_name'])) {
                $data = [
                    'student_name' => $row['student_name'],
                    'father_name' => $row['father_name'],
                    'mobile_number' => $row['mobile_number'],
                    'course' => $row['course'],
                    'class' => $row['class'],
                    'section' => $row['section'],
                    'admission_number' => $row['admission_number'],
                    'date_of_admission' => Date::excelToDateTimeObject($row['date_of_admission'])->format('Y-m-d'),
                    'mother_name' => $row['mother_name'],
                    'parent_occupation' => $row['parent_occupation'],
                    'parent_annual_income' => $row['parent_annual_income'],
                    'email' => $row['email'],
                    'gender' => $row['gender'],
                    'aadhaar_number' => $row['aadhaar_number'],
                    'caste' => $row['caste'],
                    'mother_tongue' => $row['mother_tongue'],
                    'religion' => $row['religion'],
                    'nationality' => $row['nationality'],
                    'date_of_birth' => Date::excelToDateTimeObject($row['date_of_birth'])->format('Y-m-d'),
                    'identification_marks' => $row['identification_marks'],
                    'address' => $row['address'],
                    'remarks' => $row['remarks']
                ];

                array_push($request_data, $data);
                $upload_issue = 0;
                try {
                    $student = Student::firstOrCreate(
                        [
                            'student_name' => $row['student_name'],
                            'father_name' => $row['father_name']
                        ],
                        [
                            'mobile_number' => $row['mobile_number'],
                            'branch_id' => Auth::user()->branch_id,
                            'section_id' => $this->getSectionId($row['course'], $row['class'], $row['section']),
                            'image' => 'student_dummy.png'
                        ]
                    );

                    if (!$student->wasRecentlyCreated) {
                        $student->update([
                            'mobile_number' => $row['mobile_number']
                        ]);
                    }

                    StudentDetail::updateOrCreate(
                        [
                            'student_id' => $student->id
                        ],
                        [
                            'admission_number' => $row['admission_number'],
                            'date_of_admission' => Date::excelToDateTimeObject(
                                $row['date_of_admission']
                            )->format('Y-m-d'),
                            'class_of_admission' => $this->getClassId($row['course'], $row['class']),
                            'mother_name' => $row['mother_name'],
                            'parent_occupation' => $row['parent_occupation'],
                            'parent_annual_income' => $row['parent_annual_income'],
                            'email' => $row['email'],
                            'gender' => $row['gender'],
                            'aadhaar_number' => $row['aadhaar_number'],
                            'caste' => $row['caste'],
                            'mother_tongue' => $row['mother_tongue'],
                            'religion' => $row['religion'],
                            'nationality' => $row['nationality'],
                            'date_of_birth' => Date::excelToDateTimeObject($row['date_of_birth'])->format('Y-m-d'),
                            'identification_marks' => $row['identification_marks'],
                            'address' => $row['address'],
                            'remarks' => $row['remarks']
                        ]
                    );

                    if ($student->wasRecentlyCreated) {
                        $resp_data = [
                            'student_id' => $student->id,
                            'status' => 'Enrolled Successfully'
                        ];
                    } else {
                        $resp_data = [
                            'student_id' => $student->id,
                            'status' => 'Student already Enrolled'
                        ];
                    }
                } catch (Exception $e) {
                    $resp_data = [
                        'student_name' => $row['student_name'],
                        'status' => $e->getMessage()
                    ];
                    $upload_issue++;
                }

                array_push($response_data, $resp_data);
            }
        }

        $this->import_student->request_json = json_encode($request_data);
        $this->import_student->response_json = json_encode($response_data);
        if ($upload_issue > 0) {
            $this->import_student->status = 'Issue with upload data';
        } else {
            $this->import_student->status = 'Success';
        }
    }

    /**
     * Get Section Id
     *
     * @param string $course
     * @param string $class
     * @param string $section
     * @return void
     */
    public function getSectionId($course, $class, $section)
    {
        $course_id = Course::findOrCreate($course)->id;
        $class_id = Clas::findOrCreate($class, $course_id)->id;

        return Section::findOrCreate($section, $class_id)->id;
    }

    /**
     * Get Class Id
     *
     * @param string $course
     * @param string $class
     * @return void
     */
    public function getClassId($course, $class)
    {
        $course_id = Course::findOrCreate($course)->id;

        return Clas::findOrCreate($class, $course_id)->id;
    }
}
