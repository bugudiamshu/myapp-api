<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentFee extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'student_id',
        'fee_type_id',
        'section_id',
        'course_id',
        'class_id',
        'amount',
        'is_alumni' => 0
    ];

    public $appends = ['name', 'paid', 'pending'];

    public $hidden = ['feeType'];

    public static function booted()
    {
        static::saving(function ($model) {
            $model->course_id = $model->student->section->class->course_id;
            $model->class_id = $model->student->section->class_id;
            $model->section_id = $model->student->section_id;
        });
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student', 'student_id', 'id');
    }

    public function feeType()
    {
        return $this->belongsTo('App\Models\FeeType');
    }

    public function section()
    {
        return $this->belongsTo('App\Models\Section');
    }

    public function class()
    {
        return $this->belongsTo('App\Models\Clas');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function getNameAttribute()
    {
        return $this->feeType->name;
    }

    public function getPaidAttribute()
    {
        $student_payments = StudentPayment::where('fee_type_id', $this->fee_type_id)
                                            ->where('student_id', $this->student_id)->get()->toArray();
                                    
        return array_sum(array_column($student_payments, 'paid_amount'));
    }

    public function getPendingAttribute()
    {
        $committed_fee = $this->amount;
        $paid_fee = $this->paid;
                                    
        return $committed_fee - $paid_fee;
    }
}
