<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAttendance extends Model
{
    
    protected $table = "student_attendance";
    
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'session_id',
        'date',
        'absent_json'
    ];

    public function session()
    {
        return $this->belongsTo(Session::class);
    }
}
