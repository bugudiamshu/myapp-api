<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class StudentDetail extends Model
{
    protected $fillable = [
        'student_id',
        'admission_number',
        'date_of_admission',
        'mother_name',
        'parent_occupation',
        'parent_annual_income',
        'email',
        'gender',
        'aadhaar_number',
        'caste',
        'mother_tongue',
        'religion',
        'nationality',
        'date_of_birth',
        'identification_marks',
        'address',
        'last_studied_institution',
        'noc_issued',
        'class_of_admission',
        'class_of_removal',
        'date_of_removal',
        'tc_record_sheet_number',
        'reasons_for_removal',
        'remarks'
    ];

    public $timestamps = false;

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function classOfAdmission()
    {
        return $this->belongsTo(Clas::class, 'class_of_admission');
    }

    public function classOfRemoval()
    {
        return $this->belongsTo(Clas::class, 'class_of_removal');
    }
}
