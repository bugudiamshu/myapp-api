<?php

namespace App\Models;

use App\Enums\RoleEnum;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /*
     * Trait - LogsActivity
     */
    use LogsActivity;

    use HasApiTokens;
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    // protected $guard_name = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password'
    ];

    protected $attributes = [
        'active' => true
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Determines if the User is a Super admin.
     */
    public function isSuperAdmin()
    {
        return $this->hasRole('superadmin');
    }

    /**
     * Determines if the User is a Admin.
     */
    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    /**
     * Get the owning userable model.
     */
    public function userable()
    {
        return $this->morphTo();
    }

    public function smsTemplates()
    {
        return $this->belongsToMany(SmsTemplate::class);
    }

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::saving(function ($query) {
            if (Auth::check() && !Auth::user()->isSuperAdmin()) {
                $query->branch_id = Auth::user()->branch->id;
            }
        });
    }

    public function generateUniqueUserName($username)
    {
        $username = strtolower($username);
        $variations = 0;

        while (true) {
            $newUserName = $username;
            if ($variations > 0) {
                $newUserName .= (string) $variations;
            }
            $userExist = $this->where('username', $newUserName)->exists();

            if ($userExist) {
                ++$variations;
            } else {
                $username = $newUserName;
                break;
            }
        }

        return $username;
    }
}
