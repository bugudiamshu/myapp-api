<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Student extends Model
{
    protected $hidden = [
        'course_id',
        'class_id',
        'section_id',
        'branch_id',
        'created_at',
        'updated_at',
        'image',
        'is_alumni'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id',
        'section_id',
        'student_name',
        'father_name',
        'mobile_number',
        'image'
    ];

    public $appends = [
        'total_student_fee',
        'total_paid_fee',
        'total_pending_fee',
        'course_id',
        'class_id',
        'student_image'
    ];

    public $with = [
        'studentDetails'
    ];

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function section()
    {
        return $this->belongsTo('App\Models\Section', 'section_id', 'id');
    }

    public function user()
    {
        return $this->morphOne('App\Models\User', 'userable');
    }

    public function studentFee()
    {
        return $this->hasMany('App\Models\StudentFee');
    }

    public function getTotalStudentFeeAttribute($fee_type_id = null)
    {
        if ($fee_type_id) {
            return $this->studentFee->where('fee_type_id', $fee_type_id)->sum('amount');
        }
        return $this->studentFee()->sum('amount');
        // $student_fee = $this->studentFee()->get()->toArray();
        // return array_sum(array_column($student_fee, 'amount'));
    }

    public function getTotalPaidFeeAttribute($fee_type_id = null)
    {
        if ($fee_type_id) {
            return $this->payments->where('fee_type_id', $fee_type_id)->sum('paid_amount');
        }
        return $this->payments->sum('paid_amount');
        // $student_fee = $this->payments()->get()->toArray();
        // return array_sum(array_column($student_fee, 'paid_amount'));
    }

    public function getTotalPendingFeeAttribute()
    {
        return $this->total_student_fee - $this->total_paid_fee;
    }

    public function payments()
    {
        return $this->hasMany('App\Models\StudentPayment');
    }

    public function studentDetails()
    {
        return $this->hasOne('App\Models\StudentDetail');
    }

    public function getBasicDetailsAttribute()
    {
        return [
            'id' => $this->id,
            'student_name' => $this->student_name,
            'father_name' => $this->father_name,
            'mobile_number' => $this->mobile_number,
            'image' => $this->image,

        ];
    }

    public function getCourseIdAttribute()
    {
        if ($this->first()) {
            return $this->first()->section->class->course_id;
        }
    }

    public function getClassIdAttribute()
    {
        if ($this->first()) {
            return $this->first()->section->class_id;
        }
    }

    public function getStudentImageAttribute()
    {
        return Storage::disk('student_images')->url('images/students/' . $this->image);
    }
    
    public function getCommittedFee($fee_type_id)
    {
        $committed_fee = $this->studentFee();
        if ($fee_type_id) {
            $committed_fee = $committed_fee->where('fee_type_id', $fee_type_id);
        }

        return $committed_fee->sum('amount');
    }

    public function getPaidFee($fee_type_id)
    {
        $paid_fee = $this->payments();
        if ($fee_type_id) {
            $paid_fee = $paid_fee->where('fee_type_id', $fee_type_id);
        }

        return $paid_fee->sum('paid_amount');
    }
}
