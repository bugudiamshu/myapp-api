<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clas extends Model
{
    protected $table = 'classes';

    protected $fillable = ['name', 'course_id'];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public $appends = [
        'fee_details'
    ];

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function sections()
    {
        return $this->hasMany('App\Models\Section', 'class_id', 'id');
    }

    public function classFees()
    {
        return $this->hasMany('App\Models\ClassFee', 'class_id', 'id');
    }

    public function students()
    {
        return $this->hasManyThrough('App\Models\Student', 'App\Models\Section', 'class_id', 'section_id');
    }

    public function studentFee()
    {
        return $this->hasMany('App\Models\StudentFee', 'class_id', 'id');
    }

    public function studentPayments()
    {
        return $this->hasMany('App\Models\StudentPayment', 'class_id', 'id');
    }

    public static function findOrCreate($name, $course_id)
    {
        $obj = static::where('name', 'LIKE', $name)
                    ->where('course_id', $course_id)->first();

        $model = $obj ?: new static();
        if (!isset($model->id)) {
            $model->name = $name;
            $model->course_id = $course_id;
            $model->save();
        }

        return $model;
    }

    public function getFeeDetailsAttribute()
    {
        $fee_type_ids = $this->studentFee->pluck('fee_type_id')->unique();
        $fee_details = [];
        foreach ($fee_type_ids as $fee_type_id) {
            $student_fee = $this->studentFee()->where('fee_type_id', $fee_type_id)->get();
            $student_payments = $this->studentPayments()->where('fee_type_id', $fee_type_id)->get();
            $fee_detail['name'] = $student_fee[0]['name'];
            $fee_detail['fee_type_id'] = $fee_type_id;
            $fee_detail['total_committed_fee'] = $student_fee->sum('amount');
            $fee_detail['total_paid_fee'] = $student_payments->sum('paid_amount');
            $fee_detail['total_pending_fee'] = $fee_detail['total_committed_fee'] - $fee_detail['total_paid_fee'];
            unset($this->studentFee);
            array_push($fee_details, $fee_detail);
        }

        return $fee_details;
    }

    public function getCommittedFee($fee_type_id = null)
    {
        $committed_fee = $this->studentFee();
        if ($fee_type_id) {
            $committed_fee = $committed_fee->where('fee_type_id', $fee_type_id);
        }

        return $committed_fee->sum('amount');
    }

    public function getPaidFee($fee_type_id = null)
    {
        $paid_fee = $this->studentPayments();
        if ($fee_type_id) {
            $paid_fee = $paid_fee->where('fee_type_id', $fee_type_id);
        }

        return $paid_fee->sum('paid_amount');
    }
}
