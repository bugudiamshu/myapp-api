<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Course extends Model
{
    protected $fillable = ['name', 'branch_id'];

    public function classes()
    {
        return $this->hasMany('App\Models\Clas');
    }

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::saving(function ($query) {
            if (Auth::user() && !Auth::user()->isSuperAdmin()) {
                $query->branch_id = Auth::user()->branch->id;
            }
        });
    }

    public function sections()
    {
        return $this->hasManyThrough('App\Models\Section', 'App\Models\Clas', '', 'class_id');
    }

    public static function findOrCreate($name, $branch_id = null)
    {
        $obj = static::where('name', 'LIKE', $name)
                    ->where('branch_id', $branch_id ?? Auth::user()->branch->id)->first();

        $model = $obj ?: new static();
        if (!isset($model->id)) {
            $model->name = $name;
            if ($branch_id) {
                $model->branch_id = $branch_id;
            }
            $model->save();
        }

        return $model;
    }
}
