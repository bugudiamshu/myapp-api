<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['name', 'guard'];

    public function role()
    {
        return $this->belongsToMany('App\Models\Role', 'role_has_permissions', 'permission_id', 'role_id');
    }
}
