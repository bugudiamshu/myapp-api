<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{

    public $timestamps = false;

    protected $appends = ['course_name', 'course_id', 'class_name', 'class_id'];

    protected $hidden = [
        'courseOfRemoval',
        'course_of_removal_id',
        'classOfRemoval',
        'class_of_removal_id',
        'branch_id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id',
        'course_of_removal_id',
        'class_of_removal_id',
        'branch_id',
        'batch'
    ];

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function courseOfRemoval()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function classOfRemoval()
    {
        return $this->belongsTo('App\Models\Clas');
    }

    public function getCourseNameAttribute()
    {
        return $this->courseOfRemoval->name;
    }

    public function getCourseIdAttribute()
    {
        return $this->courseOfRemoval->id;
    }

    public function getClassNameAttribute()
    {
        return $this->classOfRemoval->name;
    }

    public function getClassIdAttribute()
    {
        return $this->classOfRemoval->id;
    }
}
