<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    public function branches()
    {
        return $this->hasMany('App\Models\Branch');
    }
}
