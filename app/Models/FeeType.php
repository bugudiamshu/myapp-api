<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FeeType extends Model
{
    public $timestamps = false;

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        // auto-sets values on creation
        static::saving(function ($query) {
            if (!Auth::user()->isSuperAdmin()) {
                $query->branch_id = Auth::user()->branch->id;
            }
        });
    }

    public static function findOrCreate($name)
    {
        $obj = static::where('name', 'LIKE', $name)->first();

        $model = $obj ?: new static();
        if (!isset($model->id)) {
            $model->name = $name;
            $model->save();
        }

        return $model;
    }
}
