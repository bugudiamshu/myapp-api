<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Session extends Model
{

    public $timestamps = false;

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public static function boot()
    {
        parent::boot();

        static::saving(function ($session) {
            if (!isset($session->branch_id)) {
                $session->branch_id = Auth::user()->branch_id;
            }
        });
    }
}
