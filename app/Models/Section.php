<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public $hidden = [
        'studentDetails',
        'class',
        'students',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'class_id'
    ];

    public $with = [
        'studentFee'
    ];

    public $appends = [
        'total_committed_fee',
        'total_paid_fee',
        'total_pending_fee',
        'fee_details'
    ];

    public function class()
    {
        return $this->belongsTo('App\Models\Clas');
    }

    public static function findOrCreate($name, $class_id)
    {
        $obj = static::where('name', 'LIKE', $name)
            ->where('class_id', $class_id)->first();

        $model = $obj ?: new static();
        if (!isset($model->id)) {
            $model->name = $name;
            $model->class_id = $class_id;
            $model->save();
        }

        return $model;
    }

    public function students()
    {
        return $this->hasMany('App\Models\Student')->select(
            'id',
            'student_name',
            'father_name',
            'mobile_number',
            'image'
        )->where('is_alumni', 0);
    }

    public function getTotalCommittedFeeAttribute()
    {
        return $this->students->sum('total_student_fee');
        // return array_sum(array_column($this->students->toArray(), 'total_student_fee'));
    }

    public function getTotalPaidFeeAttribute()
    {
        return $this->students->sum('total_paid_fee');
        // return array_sum(array_column($this->students->toArray(), 'total_paid_fee'));
    }

    public function getTotalPendingFeeAttribute()
    {
        return $this->students->sum('total_pending_fee');
        // return array_sum(array_column($this->students->toArray(), 'total_pending_fee'));
    }

    public function studentFee()
    {
        return $this->hasMany('App\Models\StudentFee');
    }

    public function studentPayments()
    {
        return $this->hasMany('App\Models\StudentPayment');
    }

    public function getFeeDetailsAttribute()
    {
        $fee_type_ids = $this->studentFee->pluck('fee_type_id')->unique();
        $fee_details = [];
        foreach ($fee_type_ids as $fee_type_id) {
            $student_fee = $this->studentFee()
                                ->where('fee_type_id', $fee_type_id)
                                ->where('is_alumni', 0)
                                ->get();
            $student_payments = $this->studentPayments()
                                    ->where('fee_type_id', $fee_type_id)
                                    ->where('is_alumni', 0)
                                    ->get();
            if (isset($student_fee[0])) {
                $fee_detail['name'] = $student_fee[0]['name'];
            }
            $fee_detail['fee_type_id'] = $fee_type_id;
            $fee_detail['total_committed_fee'] = $student_fee->sum('amount');
            $fee_detail['total_paid_fee'] = $student_payments->sum('paid_amount');
            $fee_detail['total_pending_fee'] = $fee_detail['total_committed_fee'] - $fee_detail['total_paid_fee'];
            unset($this->studentFee);
            array_push($fee_details, $fee_detail);
        }

        return $fee_details;
    }

    public function getCommittedFee($fee_type_id)
    {
        $committed_fee = $this->studentFee();
        if ($fee_type_id) {
            $committed_fee = $committed_fee->where('fee_type_id', $fee_type_id);
        }

        return $committed_fee->sum('amount');
    }

    public function getPaidFee($fee_type_id)
    {
        $paid_fee = $this->studentPayments();
        if ($fee_type_id) {
            $paid_fee = $paid_fee->where('fee_type_id', $fee_type_id);
        }

        return $paid_fee->sum('paid_amount');
    }
}
