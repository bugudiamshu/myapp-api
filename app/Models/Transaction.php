<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;

class Transaction extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'branch_id',
        'transaction_type',
        'transaction_date',
        'amount',
        'purpose',
        'tag'
    ];

    public $appends = [
        'file_url'
    ];

    public function branch(): BelongsTo
    {
        return $this->belongsTo(Branch::class);
    }

    public function getFileUrlAttribute()
    {
        if ($this->file) {
            return Storage::disk('transaction_files')->url('transactions/' . $this->file);
        }

        return null;
    }

    /**
     * Limiting the transactions per branch
     *
     * @param Builder $query
     * @param Branch $branch
     * @return Builder
     */
    public function scopeForBranch(Builder $query, Branch $branch)
    {
        return $query->where('branch_id', $branch->id);
    }
}
