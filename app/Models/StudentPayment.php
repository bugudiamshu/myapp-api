<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class StudentPayment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id',
        'fee_type_id',
        'branch_id',
        'section_id',
        'course_id',
        'class_id',
        'receipt_date',
        'receipt_number',
        'paid_amount',
        'paid_by',
        'collected_by',
    ];

    public $hidden = ['feeType', 'student'];

    public $appends = ['name', 'fee_name', 'student_name'];

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function feeType()
    {
        return $this->belongsTo('App\Models\FeeType');
    }

    public function getNameAttribute()
    {
        return $this->feeType->name;
    }

    public function getFeeNameAttribute()
    {
        return 'From ' . $this->feeType->name;
    }

    public function getStudentNameAttribute()
    {
        return $this->student->student_name;
    }

    public function section()
    {
        return $this->belongsTo('App\Models\Section');
    }

    public function class()
    {
        return $this->belongsTo('App\Models\Clas');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    /**
     * Limiting the transactions per branch
     *
     * @param Builder $query
     * @param Branch $branch
     * @return Builder
     */
    public function scopeForBranch(Builder $query, Branch $branch)
    {
        return $query->where('branch_id', $branch->id);
    }
}
