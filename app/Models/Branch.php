<?php

namespace App\Models;

use App\Enums\RoleEnum;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    public function organisation()
    {
        return $this->belongsTo('App\Models\Organisation');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User', 'branch_id', 'id');
    }

    public function feeTypes()
    {
        return $this->hasMany('App\Models\FeeType');
    }

    public function courses()
    {
        return $this->hasMany('App\Models\Course');
    }

    public function sessions()
    {
        return $this->hasMany(Session::class);
    }

    public function getSectionsAttribute()
    {
        $courses = $this->courses()->has('sections')->with('sections')->get();

        return collect($courses->pluck('sections'))->collapse()->unique();
    }

    public function getClassesAttribute()
    {
        $classes = $this->courses()->has('classes')->with('classes')->get();

        return collect($classes->pluck('classes'))->collapse()->unique();
    }

    public function scopeOnlyAdmins()
    {
        return $this->users()->whereHas('roles', function ($role) {
            return $role->where('name', RoleEnum::admin()->value);
        })->get();
    }
}
