<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassFee extends Model
{
    public $timestamps = false;
    
    protected $hidden = ['pivot', 'feeType', 'class_id'];

    public $appends = ['fee_type_name'];

    public function feeType()
    {
        return $this->belongsTo('App\Models\FeeType');
    }

    public function class()
    {
        return $this->belongsTo('App\Models\Clas');
    }

    public function students()
    {
        return $this->belongsToMany('App\Models\Student');
    }

    public function getFeeTypeNameAttribute()
    {
        return $this->feeType->name;
    }
}
