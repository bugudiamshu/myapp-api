<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self transactional()
 * @method static self implicit()
 * @method static self explicit()
 */
class SmsTemplateTypeEnum extends Enum
{

}
