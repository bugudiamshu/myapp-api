<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self income()
 * @method static self expense()
 */
class TransactionTypeEnum extends Enum
{

}
