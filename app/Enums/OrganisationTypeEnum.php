<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self school()
 * @method static self college()
 */
class OrganisationTypeEnum extends Enum
{

}
