<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self superadmin()
 * @method static self admin()
 * @method static self incharge()
 * @method static self operator()
 * @method static self student()
 */
class RoleEnum extends Enum
{

}
