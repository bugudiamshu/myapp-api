<?php

namespace App\Observers;

use App\Imports\DataImport;
use App\Models\ImportStudent;
use App\Processors\BulkStudentImportProcessor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ImportStudentObserver
{
    /**
     * Handle the import student "creating" event.
     *
     * @param  \App\Models\ImportStudent  $importStudent
     * @return void
     */
    public function creating(ImportStudent $importStudent)
    {
        $importStudent->branch_id = Auth::user()->branch_id;

        if (Storage::disk('imports')->exists($importStudent->file_path)) {
            // process the uploaded file into a workable format
            $upload = storage_path('app/imports/' . $importStudent->file_path);
            Excel::import(new BulkStudentImportProcessor($importStudent), $upload);
        }
    }

    /**
     * Handle the import student "created" event.
     *
     * @param  \App\Models\ImportStudent  $importStudent
     * @return void
     */
    public function created(ImportStudent $importStudent)
    {
        //
    }

    /**
     * Handle the import student "updated" event.
     *
     * @param  \App\Models\ImportStudent  $importStudent
     * @return void
     */
    public function updated(ImportStudent $importStudent)
    {
        //
    }

    /**
     * Handle the import student "deleted" event.
     *
     * @param  \App\Models\ImportStudent  $importStudent
     * @return void
     */
    public function deleted(ImportStudent $importStudent)
    {
        //
    }

    /**
     * Handle the import student "restored" event.
     *
     * @param  \App\Models\ImportStudent  $importStudent
     * @return void
     */
    public function restored(ImportStudent $importStudent)
    {
        //
    }

    /**
     * Handle the import student "force deleted" event.
     *
     * @param  \App\Models\ImportStudent  $importStudent
     * @return void
     */
    public function forceDeleted(ImportStudent $importStudent)
    {
        //
    }
}
