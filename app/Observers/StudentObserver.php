<?php

namespace App\Observers;

use App\Models\ClassFee;
use App\Models\Section;
use App\Models\Student;
use App\Models\StudentFee;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class StudentObserver
{
    /**
     * Handle the student "creating" event.
     *
     * @param \App\Models\Student $student
     */
    public function creating(Student $student)
    {
        if (!isset($student->image) || empty($student->image)) {
            $student->image = 'student_dummy.png';
        }
    }

    /**
     * Handle the student "created" event.
     *
     * @param \App\Models\Student $student
     */
    public function created(Student $student)
    {
        $user = new User();
        $user->first_name = $student->student_name;
        $user->last_name = $student->student_name;
        $user->username = str_replace(' ', '', $user->generateUniqueUserName($student->student_name));
        $user->password = bcrypt('amshu');
        $user->branch_id = Auth::user()->branch_id;
        $user->active = 1;
        $student->user()->save($user);
        $user->assignRole('student');

        $class_id = Section::find($student->section_id)->class_id;
        $class_fees = ClassFee::where('class_id', $class_id)->get();

        try {
            foreach ($class_fees as $class_fee) {
                $student_fee = new StudentFee();
                $student_fee->student_id = $student->id;
                $student_fee->fee_type_id = $class_fee->fee_type_id;
                $student_fee->section_id = $student->section->id;
                $student_fee->class_id = $student->section->class->id;
                $student_fee->course_id = $student->section->class->course->id;
                $student_fee->amount = $class_fee->amount;
                $student_fee->save();
            }
        } catch (Exception $e) {
            Log::error("Student Observer Student Fee : " . $e->getMessage());
        }
        
    }

    /**
     * Handle the student "updating" event.
     *
     * @param \App\Models\Student $student
     */
    public function updating(Student $student)
    {
        if (!isset($student->image) || empty($student->image) || !$student->image) {
            $student->image = 'student_dummy.png';
        }
    }

    /**
     * Handle the student "updated" event.
     *
     * @param \App\Models\Student $student
     */
    public function updated(Student $student)
    {
    }

    /**
     * Handle the student "deleted" event.
     *
     * @param \App\Models\Student $student
     */
    public function deleting(Student $student)
    {
        $student->user()->forceDelete();
    }

    /**
     * Handle the student "deleting" event.
     *
     * @param \App\Models\Student $student
     */
    public function deleted(Student $student)
    {
    }

    /**
     * Handle the student "restored" event.
     *
     * @param \App\Models\Student $student
     */
    public function restored(Student $student)
    {
    }

    /**
     * Handle the student "force deleted" event.
     *
     * @param \App\Models\Student $student
     */
    public function forceDeleted(Student $student)
    {
    }
}
