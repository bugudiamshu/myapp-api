<?php

namespace App\Observers;

use App\Models\Branch;
use App\Models\Clas;
use App\Models\Course;
use App\Models\Section;
use App\Models\User;

class BranchObserver
{
    /**
     * Handle the branch "created" event.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function created(Branch $branch)
    {
        if ($branch->organisation->organisation_type == 'school') {
            $course = Course::create([
                'name'      => 'School',
                'branch_id' => $branch->id
            ]);
            $classes = ['Nursery', 'LKG', 'UKG', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];
            foreach ($classes as $class) {
                $created_class = Clas::create([
                    'name'      => $class,
                    'course_id' => $course->id
                ]);

                Section::create([
                    'name'      => 'A',
                    'class_id'  => $created_class->id
                ]);
            }
        }
        
        $user = new User();
        $user->first_name    = $branch->name;
        $user->last_name     = 'Admin';
        $user->username      = str_replace(' ', '', $user->generateUniqueUserName($branch->name));
        $user->password      = bcrypt($user->username);
        $user->branch_id     = $branch->id;
        $user->active        = 1;
        $user->save();
        $user->assignRole('admin');
    }

    /**
     * Handle the branch "updated" event.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function updated(Branch $branch)
    {
        //
    }

    /**
     * Handle the branch "deleted" event.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function deleted(Branch $branch)
    {
        //
    }

    /**
     * Handle the branch "restored" event.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function restored(Branch $branch)
    {
        //
    }

    /**
     * Handle the branch "force deleted" event.
     *
     * @param  \App\Models\Branch  $branch
     * @return void
     */
    public function forceDeleted(Branch $branch)
    {
        //
    }
}
