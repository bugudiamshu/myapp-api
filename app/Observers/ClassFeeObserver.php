<?php

namespace App\Observers;

use App\Models\ClassFee;
use App\Models\Section;
use App\Models\Student;
use App\Models\StudentFee;

class ClassFeeObserver
{
    /**
     * Handle the class fee "created" event.
     *
     * @param \App\App\Models\ClassFee $classFee
     */
    public function created(ClassFee $classFee)
    {
        $sections = Section::where('class_id', $classFee->class_id)->pluck('id')->toArray();
        $students = Student::whereIn('section_id', $sections)
                            ->where('is_alumni', 0)->get();
        foreach ($students as $student) {
            StudentFee::create([
                'student_id' => $student->id,
                'fee_type_id' => $classFee->fee_type_id,
                'section_id' => $student->section->id,
                'class_id' => $student->section->class->id,
                'course_id' => $student->section->class->course->id,
                'amount' => $classFee->amount,
                'is_alumni' => $student->is_alumni
            ]);
        }
    }

    /**
     * Handle the class fee "updated" event.
     *
     * @param \App\App\Models\ClassFee $classFee
     */
    public function updated(ClassFee $classFee)
    {
        $sections = Section::where('class_id', $classFee->class_id)->pluck('id')->toArray();
        $students = Student::whereIn('section_id', $sections)
                            ->where('is_alumni', 0)->get();
        foreach ($students as $student) {
            StudentFee::updateOrCreate(
                [
                    'student_id' => $student->id,
                    'fee_type_id' => $classFee->fee_type_id,
                ],
                [
                    'section_id' => $student->section->id,
                    'class_id' => $student->section->class->id,
                    'course_id' => $student->section->class->course->id,
                    'amount' => $classFee->amount,
                    'is_alumni' => $student->is_alumni
                ]
            );
        }
    }

    /**
     * Handle the class fee "deleted" event.
     *
     * @param \App\App\Models\ClassFee $classFee
     */
    public function deleted(ClassFee $classFee)
    {
    }

    /**
     * Handle the class fee "restored" event.
     *
     * @param \App\App\Models\ClassFee $classFee
     */
    public function restored(ClassFee $classFee)
    {
    }

    /**
     * Handle the class fee "force deleted" event.
     *
     * @param \App\App\Models\ClassFee $classFee
     */
    public function forceDeleted(ClassFee $classFee)
    {
    }
}
