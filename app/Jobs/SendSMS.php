<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendSMS implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $mobile_numbers;

    /**
     * Create a new job instance.
     *
     * @param string $message
     * @param array  $mobile_numbers
     *
     * @return void
     */
    public function __construct($message, $mobile_numbers)
    {
        $this->message        = $message;
        $this->mobile_numbers = $mobile_numbers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->mobile_numbers as $mobile_number) {
            // Send WhatsApp Message
            $sendMessageURL = sprintf(
                config('whatsapp.SEND_MESSAGE_URL') .
                '?instance_id=%s&access_token=%s&type=text&message=%s&number=%s',
                config('whatsapp.INSTANCE_ID'),
                config('whatsapp.ACCESS_TOKEN'),
                $this->message,
                '91' . $mobile_number
            );

            $response = Http::post($sendMessageURL);
            Log::info($sendMessageURL);
            if ($response->status() !== 200) {
                Log::error(sprintf('Couldn\'t send message to %s', $mobile_number));
            }
        }


        $mobile_number_sets = array_chunk($this->mobile_numbers, 50);

//        foreach ($mobile_number_sets as $mobile_number_set) {
//            // Send SMS
//            /*$response = Http::get(config('sms.GATEWAY_URL'), [
//                'user'     => config('sms.GATEWAY_USERNAME'),
//                'password' => config('sms.GATEWAY_PASSWORD'),
//                'senderid' => config('sms.SENDER_ID'),
//                'channel'  => 'Trans',
//                'number'   => implode(",", $mobile_number_set),
//                'text'     => $this->message,
//                'DCS'      => 0,
//                'flashsms' => 0
//            ]);*/
//
//
//
//        //    Log::info($response->json());
//        }
    }
}
