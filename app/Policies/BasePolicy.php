<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyPermission(['create ' . static::$key]);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function delete(User $user, $model)
    {
        if ($user->hasPermissionTo('delete ' . static::$key)) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function forceDelete(User $user, $model)
    {
        if ($user->hasPermissionTo('forceDelete ' . static::$key)) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function restore(User $user, $model)
    {
        return false;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function update(User $user, $model)
    {
        if ($user->hasPermissionTo('update ' . static::$key)) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\User $user
     *
     * @return mixed
     */
    public function view(User $user, $model)
    {
        if ($user->hasPermissionTo('view ' . static::$key)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     */
    public function viewAny(User $user)
    {
        if ($user->hasPermissionTo('view ' . static::$key)) {
            return true;
        }

        return false;
    }
}
