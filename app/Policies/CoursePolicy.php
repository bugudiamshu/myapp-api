<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;

    public $key = 'courses';
    /**
     * Determine whether the user can view the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function view(User $user, Course $course)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }

    /**
     * Determine whether the user can create coursees.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyPermission(['create ' . $this->key]);
    }

    /**
     * Determine whether the user can update the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function update(User $user, Course $course)
    {
        return $user->hasAnyPermission(['update ' . $this->key]);
    }

    /**
     * Determine whether the user can delete the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function delete(User $user, Course $course)
    {
        return $user->hasAnyPermission(['delete ' . $this->key]);
    }

    /**
     * Determine whether the user can restore the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function restore(User $user, Course $course)
    {
    }

    /**
     * Determine whether the user can permanently delete the course.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Course $course
     *
     * @return mixed
     */
    public function forceDelete(User $user, Course $course)
    {
        return $user->hasAnyPermission(['forceDelete ' . $this->key]);
    }

    /**
     * @param User $user
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }
}
