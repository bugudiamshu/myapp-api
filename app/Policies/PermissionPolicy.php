<?php

namespace App\Policies;

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public $key = 'permissions';
    /**
     * Determine whether the user can view the permission.
     *
     * @param \App\Models\User       $user
     * @param \App\Models\Permission $permission
     *
     * @return mixed
     */
    public function view(User $user, Permission $permission)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }

    /**
     * Determine whether the user can create permissions.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyPermission(['create ' . $this->key]);
    }

    /**
     * Determine whether the user can update the permission.
     *
     * @param \App\Models\User       $user
     * @param \App\Models\Permission $permission
     *
     * @return mixed
     */
    public function update(User $user, Permission $permission)
    {
        return $user->hasAnyPermission(['update ' . $this->key]);
    }

    /**
     * Determine whether the user can delete the permission.
     *
     * @param \App\Models\User       $user
     * @param \App\Models\Permission $permission
     *
     * @return mixed
     */
    public function delete(User $user, Permission $permission)
    {
        return $user->hasAnyPermission(['delete ' . $this->key]);
    }

    /**
     * Determine whether the user can restore the permission.
     *
     * @param \App\Models\User       $user
     * @param \App\Models\Permission $permission
     *
     * @return mixed
     */
    public function restore(User $user, Permission $permission)
    {
    }

    /**
     * Determine whether the user can permanently delete the permission.
     *
     * @param \App\Models\User       $user
     * @param \App\Models\Permission $permission
     *
     * @return mixed
     */
    public function forceDelete(User $user, Permission $permission)
    {
        return $user->hasAnyPermission(['forceDelete ' . $this->key]);
    }

    /**
     * @param User $user
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }
}
