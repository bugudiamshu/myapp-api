<?php

namespace App\Policies;

use App\Models\Section;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectionPolicy
{
    use HandlesAuthorization;

    public $key = 'sections';

    /**
     * Determine whether the user can view the section.
     *
     * @param \App\Models\User    $user
     * @param \App\Models\Section $section
     *
     * @return mixed
     */
    public function view(User $user, Section $section)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }

    /**
     * Determine whether the user can create sectiones.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyPermission(['create ' . $this->key]);
    }

    /**
     * Determine whether the user can update the section.
     *
     * @param \App\Models\User    $user
     * @param \App\Models\Section $section
     *
     * @return mixed
     */
    public function update(User $user, Section $section)
    {
        return $user->hasAnyPermission(['update ' . $this->key]);
    }

    /**
     * Determine whether the user can delete the section.
     *
     * @param \App\Models\User    $user
     * @param \App\Models\Section $section
     *
     * @return mixed
     */
    public function delete(User $user, Section $section)
    {
        return $user->hasAnyPermission(['delete ' . $this->key]);
    }

    /**
     * Determine whether the user can restore the section.
     *
     * @param \App\Models\User    $user
     * @param \App\Models\Section $section
     *
     * @return mixed
     */
    public function restore(User $user, Section $section)
    {
    }

    /**
     * Determine whether the user can permanently delete the section.
     *
     * @param \App\Models\User    $user
     * @param \App\Models\Section $section
     *
     * @return mixed
     */
    public function forceDelete(User $user, Section $section)
    {
        return $user->hasAnyPermission(['forceDelete ' . $this->key]);
    }

    /**
     * @param User $user
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }
}
