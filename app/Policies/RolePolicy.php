<?php

namespace App\Policies;

use App\Models\User;
use Eminiarts\NovaPermissions\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public $key = 'roles';

    protected $appends = ['test'];

    /**
     * Determine whether the user can view the role.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Role $role
     *
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        if ($user->hasRole('admin') && $role->name == 'superadmin') {
            return false;
        }

        return $user->hasAnyPermission(['view ' . $this->key]);
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyPermission(['create ' . $this->key]);
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Role $role
     *
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        if ($user->hasRole('admin') && ($role->name == 'superadmin' || $role->name == 'admin')) {
            return false;
        }

        return $user->hasAnyPermission(['update ' . $this->key]);
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Role $role
     *
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {
        if ($user->hasRole('admin') && ($role->name == 'superadmin' || $role->name == 'admin')) {
            return false;
        }

        return $user->hasAnyPermission(['delete ' . $this->key]);
    }

    /**
     * Determine whether the user can restore the role.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Role $role
     *
     * @return mixed
     */
    public function restore(User $user, Role $role)
    {
    }

    /**
     * Determine whether the user can permanently delete the role.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Role $role
     *
     * @return mixed
     */
    public function forceDelete(User $user, Role $role)
    {
        if ($user->hasRole('admin') && $role->name == 'superadmin') {
            return false;
        }

        return $user->hasAnyPermission(['forceDelete ' . $this->key]);
    }

    /**
     * @param User $user
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }
}
