<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Branch;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchPolicy
{
    use HandlesAuthorization;

    public $key = 'branches';
    /**
     * Determine whether the user can view the branch.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Branch $branch
     *
     * @return mixed
     */
    public function view(User $user, Branch $branch)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }

    /**
     * Determine whether the user can create branches.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyPermission(['create ' . $this->key]);
    }

    /**
     * Determine whether the user can update the branch.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Branch $branch
     *
     * @return mixed
     */
    public function update(User $user, Branch $branch)
    {
        return $user->hasAnyPermission(['update ' . $this->key]);
    }

    /**
     * Determine whether the user can delete the branch.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Branch $branch
     *
     * @return mixed
     */
    public function delete(User $user, Branch $branch)
    {
        return $user->hasAnyPermission(['delete ' . $this->key]);
    }

    /**
     * Determine whether the user can restore the branch.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Branch $branch
     *
     * @return mixed
     */
    public function restore(User $user, Branch $branch)
    {
    }

    /**
     * Determine whether the user can permanently delete the branch.
     *
     * @param \App\Models\User   $user
     * @param \App\Models\Branch $branch
     *
     * @return mixed
     */
    public function forceDelete(User $user, Branch $branch)
    {
        return $user->hasAnyPermission(['forceDelete ' . $this->key]);
    }

    /**
     * @param User $user
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }
}
