<?php

namespace App\Policies;

use App\Models\ClassFee;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassFeePolicy
{
    use HandlesAuthorization;

    public $key = 'class fees';

    /**
     * Determine whether the user can view the classfee.
     *
     * @param \App\Models\User     $user
     * @param \App\Models\ClassFee $classfee
     *
     * @return mixed
     */
    public function view(User $user, ClassFee $classfee)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }

    /**
     * Determine whether the user can create classfeees.
     *
     * @param \App\Models\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAnyPermission(['create ' . $this->key]);
    }

    /**
     * Determine whether the user can update the classfee.
     *
     * @param \App\Models\User     $user
     * @param \App\Models\ClassFee $classfee
     *
     * @return mixed
     */
    public function update(User $user, ClassFee $classfee)
    {
        return $user->hasAnyPermission(['update ' . $this->key]);
    }

    /**
     * Determine whether the user can delete the classfee.
     *
     * @param \App\Models\User     $user
     * @param \App\Models\ClassFee $classfee
     *
     * @return mixed
     */
    public function delete(User $user, ClassFee $classfee)
    {
        return $user->hasAnyPermission(['delete ' . $this->key]);
    }

    /**
     * Determine whether the user can restore the classfee.
     *
     * @param \App\Models\User     $user
     * @param \App\Models\ClassFee $classfee
     *
     * @return mixed
     */
    public function restore(User $user, ClassFee $classfee)
    {
    }

    /**
     * Determine whether the user can permanently delete the classfee.
     *
     * @param \App\Models\User     $user
     * @param \App\Models\ClassFee $classfee
     *
     * @return mixed
     */
    public function forceDelete(User $user, ClassFee $classfee)
    {
        return $user->hasAnyPermission(['forceDelete ' . $this->key]);
    }

    /**
     * @param User $user
     */
    public function viewAny(User $user)
    {
        return $user->hasAnyPermission(['view ' . $this->key]);
    }
}
