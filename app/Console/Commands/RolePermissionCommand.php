<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign given permissions to system roles';

    /**
     * [$permissions description].
     *
     * @var [type]
     */
    protected $permissions;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $roles = Role::all();
        $this->permissions = Permission::all();
        $dashboard = $this->getPermission('Dashboard');
        $my_account = $this->getPermission('My Account');
        $create_student = $this->getPermission('Create Student');
        $update_student = $this->getPermission('Update Student');
        $delete_student = $this->getPermission('Delete Student');
        $search_student = $this->getPermission('Search Student');

        $assigned = [
            $dashboard->id => ['roles' => '*'],
            $my_account->id => ['roles' => '*'],
            $create_student->id => ['roles' => ['superadmin', 'admin', 'operator']],
            $update_student->id => ['roles' => ['superadmin', 'Admin', 'operator']],
            $delete_student->id => ['roles' => ['superadmin', 'Admin', 'operator']],
            $search_student->id => ['roles' => ['superadmin', 'Admin', 'operator']],
        ];

        $this->store($assigned, $this->permissions);
    }

    /**
     * search for and retrieve a given permission by name.
     *
     * @param string $permission the permission label to search for
     *
     * @return App\Models\Permission $permission
     */
    private function getPermission($permission)
    {
        return $this->permissions->where('name', $permission)->first();
    }

    /**
     * store all of the permission definitions.
     *
     * @param array      $assigned    a list of permission definitions to store
     * @param Collection $permissions all available system permissions
     */
    protected function store(array $assigned, $permissions)
    {
        foreach ($assigned as $key => $assign) {
            $permission = $this->filter($key, $permissions);

            /*
             * if set to * [ALL] then grant access to all groups for this permission
             */
            if ($assign['roles'] === '*') {
                $permission->assignRole(Role::all());
                continue;
            }

            $permission->assignRole($assign['roles']);
        }
    }

    /**
     * search through a list of supplied permissions and return by the permission string.
     *
     * @param string $type the type of permission to search for
     *
     * @return App\Models\Permission
     */
    private function filter($permissionId, $permissions)
    {
        return $permissions->filter(function ($permission) use ($permissionId) {
            return $permission->id === $permissionId;
        })->first();
    }
}
