<?php

namespace App\Console\Commands;

use App\Imports\DataImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportGandhiNagarStudentsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:students';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Gandhi Nagar Students';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Importing spreadsheet data');
        Excel::import(new DataImport(), 'app/data/imports/students_gandhinagar.xlsx');
        $this->info('Data imported successfully');
    }
}
