<?php

namespace App\Console\Commands;

use App\Enums\RoleEnum;
use App\Jobs\SendSMS;
use App\Models\User;
use App\Services\PrintService;
use App\Services\SMSService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendDailyTransactionReportSMSCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:transactions_today';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends an SMS with an overview of today\'s transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::whereHas('roles', function ($role) {
            return $role->where('name', RoleEnum::admin());
        })->get();

        $today = Carbon::now();
        foreach ($users as $user) {
            $transactions = (new PrintService())->setUser($user)
                ->filterTransactions(
                    'both',
                    [
                        $today->format('Y-m-d'),
                        $today->format('Y-m-d')
                    ],
                    'all'
                );
            if ($transactions['total_income'] || $transactions['total_expense']) {
                SMSService::sendDailyTransactionSummary(
                    $user,
                    $today->format('d/m/Y'),
                    $transactions['total_income'],
                    $transactions['total_expense']
                );
            }
        }
    }
}
