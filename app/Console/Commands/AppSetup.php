<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class AppSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make app setup from scratch';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('This process will:');
        $this->info('- Run all migrations');
        $this->info('- Run all seeders');
        $this->info('- Clear all cache');
        // $this->info('- Generate all roles and permissions');

        if ($this->confirm('Do you wish to continue?')) {
            $this->build();
        }
    }

    /**
     * setup the app.
     *
     * @param array $person
     */
    private function build()
    {
        //if (config('app.env') == 'local') {
            $bar = $this->output->createProgressBar(3);

            // 1
            $this->info('Setting up the database');
            $colname = 'Tables_in_' . env('DB_DATABASE');
            $tables = DB::select('SHOW TABLES');
            foreach ($tables as $table) {
                $droplist[] = $table->$colname;
            }
            if (!empty($droplist)) {
                $droplist = implode(',', $droplist);
                $this->info('Dropping all existing tables');
                DB::beginTransaction();
                //turn off referential integrity
                DB::statement('SET FOREIGN_KEY_CHECKS = 0');
                DB::statement("DROP TABLE $droplist");
                //turn referential integrity back on
                DB::statement('SET FOREIGN_KEY_CHECKS = 1');
                DB::commit();
            }

            $this->call('migrate:refresh');
            $bar->advance();

            // 2
            $this->info('Seeding the database');
            $this->call('db:seed');
            $bar->advance();

            //3
            $this->info('Clearing the cache(s)');
            $this->call('config:clear');
            $this->call('cache:clear');
            $this->call('view:clear');
            $bar->advance();
        // } else {
        //     $this->error('Application is not local. Unable to continue.');
        //     exit;
        // }
    }
}
