<?php

namespace App\Console\Commands;

use App\Jobs\SendSMS;
use App\Models\StudentDetail;
use App\Services\SMSService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendBirthdaySMSCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        StudentDetail::where('date_of_birth', Carbon::today()->format('Y-m-d'))
            ->select('student_id')
            ->get()
            ->map(function ($student_detail) {
                SMSService::sendBirthdaySMS(
                    $student_detail->student->student_name,
                    $student_detail->student->mobile_number
                );
            });
    }
}
