<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;

class MethodNotFoundException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        Log::error(get_class());
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response("Method not found Exception", 400);
    }
}
