<?php

namespace MyAppApi\TotalClassFee;

use App\Models\ClassFee;
use Illuminate\Http\Request;
use Laravel\Nova\Card;

class TotalClassFee extends Card
{
    /**
     * The width of the card (1/3, 1/2, or full).
     *
     * @var string
     */
    public $width = '1/5';

    /**
     * Get the component name for the element.
     *
     * @return string
     */
    public function component()
    {
        return 'total-class-fee';
    }

    public function calculateClassFee(Request $request, $resourceId = null)
    {
        $classfees = ClassFee::where('class_id', $resourceId)->get();

        $totalFee = 0;
        foreach ($classfees as $classfee) {
            $totalFee += $classfee->amount;
        }

        return $this->withMeta([
          'totalFee' => $totalFee,
        ])->onlyOnDetail();
    }
}
