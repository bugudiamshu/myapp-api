@extends('pdf.fee_details.master')

@section('header')
Audit Report for {{ $tag }} transactions from {{ \Carbon\Carbon::parse($from_date)->format('d/m/Y') }} to {{ \Carbon\Carbon::parse($to_date)->format('d/m/Y') }}
@endsection

@section('report')
<table class="blueTable">
   <thead>
      <tr>
         <th>S.No</th>
         <th>Transaction Date</th>
         <th>Purpose</th>
         <th>Income</th>
         <th>Expense</th>
      </tr>
   </thead>
   <tbody>
        @foreach ($transactions as $transaction)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>@if ($transaction['transaction_date'])
                    {{ \Carbon\Carbon::parse($transaction['transaction_date'])->format('d-m-Y') }}
                @endif
            </td>
            <td>{{ $transaction['purpose'] }}</td>
            <td>{{ $transaction['transaction_type'] == 'income' ? $transaction['amount'] : 0 }}</td>
            <td>{{ $transaction['transaction_type'] == 'expense' ? $transaction['amount'] : 0 }}</td>
        </tr>
        @endforeach
   </tbody>
   <tr class="table-footer">
        <td colspan="3">
        <span style="font-weight: bold">Total</span>
        </td>
        <td> {{ $total_income }} </td>
        <td> {{ $total_expense }} </td>
    </tr>
    <tr class="table-footer">
        <td colspan="4">
        <span style="font-weight: bold">Remaining Amount</span>
        </td>
        <td> {{ $total_income - $total_expense }} </td>
    </tr>

</table>
@endsection