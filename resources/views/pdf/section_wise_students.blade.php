@extends('pdf.fee_details.master')

@section('header')
Showing List of Students <br/>
<span style="text-decoration: none">Course: {{ $course }} || Class: {{ $class }} || Sections: {{ $sections }}</span>
@endsection

@section('report')
<table class="blueTable">
   <thead>
      <tr>
         <th>S.No</th>
         <th>Section</th>
         @foreach ($column_names as $column_name)
         <th>{{ str_replace("_", " ", \Illuminate\Support\Str::title($column_name)) }}</th>
         @endforeach
         
      </tr>
   </thead>
   <tbody>
        @foreach ($students as $student)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $student['section'] }}</td>
            @foreach ($column_names as $column_name)
            <td>{{ $student[$column_name] }}</td>
            @endforeach
        </tr>
        @endforeach
   </tbody>

</table>
@endsection