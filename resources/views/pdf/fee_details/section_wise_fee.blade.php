@extends('pdf.fee_details.master')

@section('header')
Section Wise Fee Report
@endsection

@section('report')
<table class="blueTable">
   <thead>
      <tr>
         <th>S.No</th>
         <th>Student Name</th>
         <th>Father Name</th>
         <th>Committed Fee</th>
         <th>Paid Fee</th>
         <th>Pending Fee</th>
      </tr>
   </thead>
   <tbody>
        @foreach ($students as $student)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $student->student_name }}</td>
            <td>{{ $student->father_name }}</td>
            <td>{{ $student->committed_fee }}</td>
            <td>{{ $student->paid_fee }}</td>
            <td>{{ $student->committed_fee - $student->paid_fee }}</td>
        </tr>
        @endforeach
   </tbody>
   <tr class="table-footer">
        <td colspan="3">
        <span style="font-weight: bold">Total</span>
        </td>
        <td> {{ $total_committed_fee }} </td>
        <td> {{ $total_paid_fee }} </td>
        <td> {{ $total_committed_fee - $total_paid_fee }} </td>
    </tr>

</table>
@endsection