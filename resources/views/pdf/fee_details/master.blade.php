<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content-center {
                text-align: center;
            }

            .print_header {
                text-align: center;
                font-weight: bold;
                font-size: 18px;
                margin-bottom: 10px;
                text-decoration: underline;
                letter-spacing: .1rem;
            }
            .course-detail {
                font-size: 16px;
            }
            .course-detail-label {
                font-weight: bold;
            }
            .report {
                margin-top: 20px;
            }

            .student-details {
                align-items: left;
                display: flex;
                justify-content: left;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <br/>
            <div class="print_header" style="text-align:center">
                @yield('header')
            </div>
            <div class="course-detail content-center">
            @if (isset($course_detail['course_name']))
                <span>Course : <span class="course-detail-label"> {{ $course_detail['course_name'] }}  </span> </span>
            @endif

            @if (isset($course_detail['class_name']))
                <span> | Class : <span class="course-detail-label"> {{ $course_detail['class_name'] }} </span> </span>
            @endif

            @if (isset($course_detail['section_name']))
                <span> | Section : <span class="course-detail-label"> {{ $course_detail['section_name'] }} </span> </span>
            @endif
            </div>
            <div class="content-center">
                @yield('student_details')
            </div>
            <div class="content-center report">
                @yield('report')
            </div>
            
        </div>
    </body>
</html>