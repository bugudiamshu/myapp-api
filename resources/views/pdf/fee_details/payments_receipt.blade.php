@extends('pdf.fee_details.master')

@section('header')
PAYMENT RECEIPT
@endsection

@section('report')
<table class="blueTable">
   <thead>
         <tr>
            <th  style="text-align:left" colspan="7">
               Student Name : {{ $student_detail['student_name'] }} <br/><br/>
               Class: {{ $student_detail['class_name'] }}<br/><br/>
               Section: {{ $student_detail['section_name'] }}
            </th>
         </tr>
         <tr>
            <th colspan="6">
               <span style="font-weight: bold; font-size: 18px">{{ $fee_name }}</span>
            </th>
            <th><span style="font-weight: bold; font-size: 18px">{{ $committed_fee }}</span></th>
         </tr>
      <tr>
         <th>S.No</th>
         <th>Date</th>
         <th>Fee Type</th>
         <th>Receipt Number</th>
         <th>Paid By</th>
         <th>Collected By</th>
         <th>Paid</th>
      </tr>
   </thead>
   <tbody>
        @foreach ($payments as $payment)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $payment['receipt_date'] }}</td>
            <td>{{ $payment['fee_name'] }}</td>
            <td>{{ $payment['receipt_number'] }}</td>
            <td>{{ $payment['paid_by'] }}</td>
            <td>{{ $payment['collected_by'] }}</td>
            <td>{{ $payment['paid_amount'] }}</td>
        </tr>
        @endforeach
   </tbody>
   <tr class="table-footer">
        <td colspan="6">
        <span style="font-weight: bold">Total Paid</span>
        </td>
        <td> {{ $paid_total }} </td>
    </tr>
    <tr class="table-footer">
        <td colspan="6">
        <span style="font-weight: bold">Pending Fee</span>
        </td>
        <td> {{ $pending_fee }} </td>
    </tr>

</table>
@endsection