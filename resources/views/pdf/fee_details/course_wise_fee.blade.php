@extends('pdf.fee_details.master')

@section('header')
Course Wise Fee Report
@endsection

@section('report')
<table class="blueTable">
   <thead>
      <tr>
         <th>S.No</th>
         <th>Class Name</th>
         <th>Committed Fee</th>
         <th>Paid Fee</th>
         <th>Pending Fee</th>
      </tr>
   </thead>
   <tbody>
        @foreach ($classes as $class)
        <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $class->name }}</td>
            <td>{{ $class->committed_fee }}</td>
            <td>{{ $class->paid_fee }}</td>
            <td>{{ $class->committed_fee - $class->paid_fee }}</td>
        </tr>
        @endforeach
   </tbody>
   <tr class="table-footer">
        <td colspan="2">
        <span style="font-weight: bold">Total</span>
        </td>
        <td> {{ $total_committed_fee }} </td>
        <td> {{ $total_paid_fee }} </td>
        <td> {{ $total_committed_fee - $total_paid_fee }} </td>
    </tr>

</table>
@endsection