<div class="header">
    <div class="organisation">
        <span style="color:red; font-size: 27px;letter-spacing: .1rem; ">
            {{ $organisation_name }} </span> <br />
        <span style="font-size: 14px;">(Recognised by Govt. of T.S.)</span> <br />
        <span style="font-size: 16px;">{{ $branch_name }}, Vikarabad Dist., Telangana 501141</span>
    </div>
    <hr style="border:2px solid black" />
</div>