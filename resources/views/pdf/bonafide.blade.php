<?php

$date_of_admission = \Carbon\Carbon::createFromFormat(
    'Y-m-d',
    $student->studentDetails->date_of_admission
)->format('Y');
$date_of_admission = $date_of_admission - 1 . '-' . $date_of_admission;

$date_of_removal = $student->studentDetails->date_of_removal ?
    \Carbon\Carbon::createFromFormat('Y-m-d', $student->studentDetails->date_of_removal)->format('Y') :
    \Carbon\Carbon::now()->format('Y');
$date_of_removal = $date_of_removal - 1 . '-' . $date_of_removal;

$date_of_birth = \Carbon\Carbon::createFromFormat(
    'Y-m-d',
    $student->studentDetails->date_of_birth
);

$date_of_birth_year_string = (new NumberFormatter('en', NumberFormatter::SPELLOUT))
    ->format($date_of_birth->format('Y'));
$date_of_birth_month_string = (new NumberFormatter('en', NumberFormatter::SPELLOUT))
    ->format($date_of_birth->format('m'));
$date_of_birth_day_string = (new NumberFormatter('en', NumberFormatter::SPELLOUT))
    ->format($date_of_birth->format('d'));
?>

<style>
.dynamic-values {
    color:blue;
    font-weight:bold;
    text-decoration:underline;
    font-style:normal;
}
</style>

<div class="header" style="border: 4px solid black;border-style:dashed">
    <div class="organisation">
        <span style="color:red; font-size: 33px;letter-spacing: .1rem; ">
            {{ $student->user->branch->organisation->name }} </span> <br />
        <span style="font-size: 14px;">(Recognised by Govt. of T.S.)</span> <br />
        <span style="font-size: 16px;">{{ $student->user->branch->name }}, Vikarabad Dist., Telangana 501141</span>
    </div>
    <hr style="border:2px solid black" />
    <div style="text-align: center; margin-top: 20px;">
        <span style="letter-spacing: .1rem; text-align:center; 
        width:230px; font-size: 22px;font-weight: bold; border: 4px solid red; color:red">
            &nbsp;BONAFIDE CERTIFICATE&nbsp;
        </span>
    </div>
    <br />
    <div style="margin-left:10px;margin-right:10px;font-size: 20px; 
    font-style:italic; letter-spacing:.1rem;padding-left:20px;padding-right:20px;">
        <div style="float: left; width: 50%;text-align:left">
            Admission No. <span style="color:red; font-style:normal;font-weight: bold">
                {{ $student->studentDetails->admission_number }}</span>
        </div>

        <div style="float: right; width: 50%;text-align:right">
            Date: <span style="color: blue;text-decoration:underline;font-style:normal;font-weight:bold">
                {{ \Carbon\Carbon::now()->format('d/m/Y') }}</span>
        </div>

        <br />

        <div style="text-align:left;margin-top:-20px;">
            <p style="line-height:50px;text-indent: 100px;">
                This is to certify that Master/Kumar
                <span class="dynamic-values">
                    {{ $student->student_name }}</span>
                S/o, D/o
                <span class="dynamic-values">
                    {{ $student->father_name }}</span>
                is/was a Bonafide student of this Institution. He/She studying / studied from Class
                <span class="dynamic-values">
                    {{ $student->studentDetails->classOfAdmission->name }}</span>
                Year
                <span class="dynamic-values">
                    {{ $date_of_admission }}</span>
                to Class
                <span class="dynamic-values">
                    {{ $student->studentDetails->class_of_removal ?
                        $student->studentDetails->classOfRemoval->name : $student->section->class->name}}</span>
                Year
                <span class="dynamic-values">
                    {{ $date_of_removal }}</span>
                During his/her study period his/her character was found Good.
            </p>

            <p>
                His/her date of birth according to School Admisison Register is
                <span class="dynamic-values">
                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', 
                        $student->studentDetails->date_of_birth)->format('d-m-Y') }}<br/>
                    ({{ \Illuminate\Support\Str::title($date_of_birth_day_string . ' ' . $date_of_birth_month_string . 
                        ' ' . $date_of_birth_year_string) }})</span>
            </p>
        </div>

        <div style="text-align:right;margin-top:100px;font-weight:bold">
            Head Master / Principal
        </div>
        <br />
    </div>







</div>