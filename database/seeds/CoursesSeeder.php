<?php

use App\Models\Course;
use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder
{
    protected $courses = [
        1 => [
             'name' => 'Course 1',
             'branch_id' => 1,
         ],
     ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('courses')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($this->courses as $key => $value) {
            $course = Course::create($value);
            $course->save();
        }
    }
}
