<?php

use App\Models\Clas;
use Illuminate\Database\Seeder;

class ClassesSeeder extends Seeder
{
    protected $classes = [
        1 => [
             'name' => 'Class 1',
             'course_id' => 1,
         ],
     ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('classes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($this->classes as $key => $value) {
            $class = Clas::create($value);
            $class->save();
        }
    }
}
