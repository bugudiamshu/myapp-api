<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    protected $users = [
       0 => [
            'first_name' => 'Amshu',
            'last_name' => 'Kanth',
            'username' => 'superadmin',
            'password' => 'superadmin',
            'active' => 1,
       ]
    ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        $createdusers = [];
        foreach ($this->users as $key => $value) {
            $value['password'] = bcrypt($value['password']);
            User::create($value);
            $createdusers[] = $value;
        }

        print_r($createdusers);
    }
}
