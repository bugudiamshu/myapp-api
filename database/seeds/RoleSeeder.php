<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * list of roles to seed.
     *
     * @var array
     */
    protected $roles = [
        ['name' => 'superadmin'],
        ['name' => 'admin'],
        ['name' => 'operator'],
        ['name' => 'staff'],
        ['name' => 'parent'],
        ['name' => 'student'],
    ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        foreach ($this->roles as $key => $role) {
            DB::table('roles')->insert([
                'name' => $role['name'],
                'guard_name' => 'api',
            ]);
        }
    }
}
