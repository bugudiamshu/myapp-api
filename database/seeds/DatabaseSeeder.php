<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        if (config('app.env') == 'local') {
            $this->call([
                // OrganisationTableSeeder::class,
                // BranchesSeeder::class,
                // PermissionsSeeder::class,
                // RoleSeeder::class,
                UsersSeeder::class,
                RolesAndPermissionsSeeder::class,
                // CoursesSeeder::class,
                // ClassesSeeder::class,
                // SectionsSeeder::class,
            ]);
        } else {
            $this->call([
                // OrganisationTableSeeder::class,
                // BranchesSeeder::class,
                UsersSeeder::class,
                RolesAndPermissionsSeeder::class,
            ]);
        }
    }
}
