<?php

use App\Models\Section;
use Illuminate\Database\Seeder;

class SectionsSeeder extends Seeder
{
    protected $sections = [
        1 => [
             'name' => 'Section 1',
             'class_id' => 1,
         ],
     ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('sections')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($this->sections as $key => $value) {
            $section = Section::create($value);
            $section->save();
        }
    }
}
