<?php

use Illuminate\Database\Seeder;
use App\Models\Organisation;

class OrganisationTableSeeder extends Seeder
{
    protected $organisations = [
        [
            'name' => 'Brilliant Convent High School',
        ],
     ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('organisations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($this->organisations as $key => $value) {
            $organisation = Organisation::create($value);
            $organisation->save();
        }
    }
}
