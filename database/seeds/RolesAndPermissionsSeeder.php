<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('permissions')->truncate();
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $collection = collect([
            'users',
            'roles',
            'permissions',
            'branches',
            'courses',
            'classes',
            'sections',
            'students',
            'class fees',
            'student fee',
            'payments'
            // 'teams',
            // ... // List all your Models you want to have Permissions for.
        ]);
        // Create a Super-Admin Role and assign all Permissions
        $role = Role::create(['name' => 'superadmin']);

        $collection->each(function ($item, $key) {
            // create permissions for each collection item
            Permission::create(['group' => $item, 'name' => 'view '.$item]);
            Permission::create(['group' => $item, 'name' => 'view own '.$item]);
            Permission::create(['group' => $item, 'name' => 'create '.$item]);
            Permission::create(['group' => $item, 'name' => 'update '.$item]);
            Permission::create(['group' => $item, 'name' => 'delete '.$item]);
            //Permission::create(['group' => $item, 'name' => 'manage own '.$item]);
            // Permission::create(['group' => $item, 'name' => 'restore '.$item]);
            Permission::create(['group' => $item, 'name' => 'forceDelete '.$item]);
        });

        $role->givePermissionTo(Permission::all());

        // Give User Super-Admin Role
        $user = App\Models\User::where('username', 'superadmin')->first();
        $user->assignRole('superadmin');

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());
        //$user = App\Models\User::where('username', 'admin')->first();
        //$user->assignRole('admin');

        Role::create(['name' => 'student']);
        Role::create(['name' => 'staff']);
        Role::create(['name' => 'parent']);
    }
}
