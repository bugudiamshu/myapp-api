<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * list of permissions to seed.
     *
     * @var array
     */
    protected $permissions = [
        'everything',
        'createRole', 'updateRole', 'viewRole', 'deleteRole',
        'createBranch', 'updateBranch', 'viewBranch', 'deleteBranch', 'searchBranch',
    ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        foreach ($this->permissions as $permission) {
            DB::table('permissions')->insert([
                'name' => $permission,
                'guard_name' => 'api',
            ]);
        }
    }
}
