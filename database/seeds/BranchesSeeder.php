<?php

use Illuminate\Database\Seeder;
use App\Models\Branch;

class BranchesSeeder extends Seeder
{
    protected $branches = [
        1 => [
             'name' => 'Gandhi Nagar',
             'organisation_id' => 1,
         ],
        2 => [
            'name' => 'Old Tandur',
            'organisation_id' => 1,
        ],
        3 => [
            'name' => 'Basheerabad',
            'organisation_id' => 1,
        ],
        4 => [
            'name' => 'Rajeev Gruha Kalpa',
            'organisation_id' => 1,
        ],
        5 => [
            'name' => 'College',
            'organisation_id' => 1,
        ],
     ];

    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('branches')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        foreach ($this->branches as $key => $value) {
            $branch = Branch::create($value);
            $branch->save();
        }
    }
}
