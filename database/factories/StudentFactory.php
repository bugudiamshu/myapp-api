<?php

use App\Models\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'student_name' => $faker->name(),
        'father_name' => $faker->name(),
        'admission_no' => $faker->numberBetween(0, 1000),
        'roll_no' => $faker->numberBetween(0, 100),
        'mobile_number' => $faker->numberBetween(8000000000, 9999999999),
        'section_id' => 1,
        'branch_id' => 1,
    ];
});
