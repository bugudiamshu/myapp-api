<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('student_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->string('admission_number')->nullable();
            $table->date('date_of_admission')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('parent_occupation')->nullable();
            $table->string('parent_annual_income')->nullable();
            $table->string('aadhaar_number')->nullable();
            $table->string('email')->nullable();
            $table->string('gender')->nullable();
            $table->string('caste')->nullable();
            $table->string('mother_tongue')->nullable();
            $table->string('nationality')->nullable();
            $table->string('religion')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->text('identification_marks')->nullable();
            $table->text('address')->nullable();
            $table->string('last_studied_institution')->nullable();
            $table->boolean('noc_issued')->nullable();
            $table->integer('class_of_admission')->unsigned()->nullable();
            $table->foreign('class_of_admission')->references('id')->on('classes')->onDelete('cascade');
            $table->integer('class_of_removal')->unsigned()->nullable();
            $table->foreign('class_of_removal')->references('id')->on('classes')->onDelete('cascade');
            $table->date('date_of_removal')->nullable();
            $table->string('tc_record_sheet_number')->nullable();
            $table->text('reasons_for_removal')->nullable();
            $table->text('remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('student_details');
    }
}
