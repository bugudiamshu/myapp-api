@servers(['web' => ['brilliant']])

@task('deploy-backend', ['on' => 'web'])
    @if ($branch)
        cd ~/public_html/{{$branch}}/api
        git reset
        git checkout .
        git config core.filemode false
        git pull origin {{ $branch }}
    @else
        cd public_html/prod-api
        git reset
        git checkout .
        git config core.filemode false
        git pull origin master
    @endif
    COMPOSER_MEMORY_LIMIT=-1 composer update && composer dump-autoload
    php artisan migrate
    php artisan view:clear
    php artisan route:clear
    php artisan cache:clear
    php artisan config:clear
    echo -e "*****Completed Backend for {{$branch}} Deployment*****"
@endtask

@task('deploy-frontend', ['on' => 'web'])
    @if ($branch == 'dev')
        cd ~/public_html/dev/myapp-frontend
        git reset
        git checkout .
        git pull origin {{ $branch }}
        npm install
        ng build --output-path="../frontend" --configuration=dev
    @elseif ($branch == 'pre-prod')
        cd ~/public_html/pre-prod/myapp-frontend
        git reset
        git checkout .
        git pull origin {{ $branch }}
        npm install
        ng build --output-path="../frontend" --configuration=pre-production
    @else
        cd public_html/prod-frontend
        git reset
        git checkout .
        git pull origin master
        npm install
        ng build --prod --configuration=production
    @endif
    echo -e "*****Completed Frontend for {{$branch}} Deployment*****"
@endtask
