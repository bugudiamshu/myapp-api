
if [[ "$1" == "dev-api" ]]; then
    
  envoy run deploy-ec2-backend --branch=dev
  
  echo "Update of backend for staging completed"
fi

if [[ "$1" == "deploy-frontend" ]]; then
    
  envoy run deploy-frontend master
  
  echo "Update of frontend for staging completed"
fi