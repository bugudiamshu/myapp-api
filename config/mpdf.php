<?php

return [
    'margin_left'       => 10,
    'margin_right'      => 10,
    'margin_top'        => 35,
    'margin_bottom'     => 15,
    'margin_header'     => 10,
    'margin_footer'     => 10,
    'orientation'       => 'L',
    'tempDir'           => '/tmp'
];
