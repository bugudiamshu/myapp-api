<?php

return [
    'APP_URL' => env('APP_URL'),
    'CACHE_EXPIRE' => env('CACHE_EXPIRE'),
    'ERRORS' => [
        500 => 'Something went wrong',
        400 => 'Bad Request',
        404 => 'Resource Not found',
        422 => 'Invalid Request'
    ],
    'MESSAGES' => [
        'CREATED' => 'Saved successfully',
        'UPDATED' => 'Updated successfully',
        'DELETED' => 'Deleted successfully',
        'UPLOAD_SUCCESS' => 'Successfully Uploaded'
    ]
];
