<?php

return [
    'GATEWAY_URL'           => env('SMS_GATEWAY_URL'),
    'GATEWAY_USERNAME'      => env('SMS_GATEWAY_USERNAME'),
    'GATEWAY_PASSWORD'      => env('SMS_GATEWAY_PASSWORD'),
    'SENDER_ID'             => env('SMS_SENDER_ID'),
    'PAYMENT_RECEIPT'       => 'Dear Parent, we have received your ward %s School Fee of Rs.%s/- on %s bearing Receipt No.%s.Pending Balance is Rs.%s/-.'. "\n". 'Regards,' . "\n" . ' Brilliant Convent School',
    'DAY_TRANSACTIONS'      => 'Dear %s, On %s Total income is %s and Total expenditure is %s for branch %s - BRILLIANT SCHOOL'
];
