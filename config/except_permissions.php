<?php

return [
    'admin' => [
        'forceDelete users',
        'forceDelete courses',
        'forceDelete classes',
        'forceDelete sections',
        'forceDelete students',
        'forceDelete class fees',
    ],
];
