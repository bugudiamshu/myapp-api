<?php

return [
    'INSTANCE_ID' => env('WHATSAPP_INSTANCE_ID'),
    'ACCESS_TOKEN' => env('WHATSAPP_ACCESS_TOKEN'),
    'SEND_MESSAGE_URL' => 'https://wa.intractly.com/api/send'
];
