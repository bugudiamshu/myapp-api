<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class StudentTest extends TestCase
{
    use WithoutMiddleware;

    public $header = ['Content-Type' => 'application/json', 'Accept' => 'application/json'];

    /**
     * A basic unit test example.
     */
    public function testStudentCreate()
    {
        $user = $this->userLogin(['username' => 'admin', 'password' => 'adminb1']);
        $faker = Faker::create();
        $student = array(
            'student_name' => $faker->name,
            'father_name' => $faker->name,
            'admission_no' => $faker->numberBetween(0, 1000),
            'roll_no' => $faker->numberBetween(0, 100),
            'mobile_number' => $faker->numberBetween(8000000000, 9999999999),
            'branch_id' => 9,
            'section_id' => 1,
        );

        $response = $this->json('POST', 'api/v1/student', $student, $this->header);
        $response->assertStatus(422);

        $student['branch_id'] = $user['branch_id'];
        $student['section_id'] = 999;
        $response = $this->json('POST', 'api/v1/student', $student, $this->header);
        $response->assertStatus(422);

        $student['branch_id'] = 2;
        $student['section_id'] = 1;
        // $this->expectOutputString($user['branch_id'].' '.$student['branch_id']);
        $response = $this->json('POST', 'api/v1/student', $student, $this->header);
        $response->assertStatus(406);

        $user = $this->userLogin(['username' => 'operator', 'password' => 'operator']);

        $student['branch_id'] = 2;
        $student['section_id'] = 1;
        $response = $this->json('POST', 'api/v1/student', $student, $this->header);
        $response->assertStatus(406);

        // $branch_id = $user['branch_id'];
        // $section_id = Auth::user()->branch->courses()->first()->classes()->first()->sections()->first()->id;

        //$student = factory(\App\Models\Student::class)->create(['branch_id' => $branch_id, 'section_id' => $section_id]);

        // $student = array(
        //     'student_name' => $faker->name(),
        //     'father_name' => $faker->name(),
        //     'admission_no' => $faker->numberBetween(0, 1000),
        //     'roll_no' => $faker->numberBetween(0, 100),
        //     'mobile_number' => $faker->numberBetween(8000000000, 9999999999),
        //     'section_id' => 1,
        //     'branch_id' => 1,
        // );

        // //$response = $this->json('POST', 'v1/student', $student, $this->header);
        // $this->expectedOutput($student);
    }

    public function userLogin($data)
    {
        $userInfo = $this->json('POST', '/api/v1/login', $data, $this->header)->decodeResponseJson();
        $this->header['token'] = 'Bearer '.$userInfo['token'];

        return $userInfo['user_details'];
    }
}
