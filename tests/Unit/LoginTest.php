<?php

namespace Tests\Unit;

use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic unit test example.
     */
    public function testLogin()
    {
        $this->assertInvalidCredentials([]);
        $this->assertInvalidCredentials(['username' => 'test', 'password' => 'test']);
        $this->assertCredentials(['username' => 'operator', 'password' => 'operator']);
    }
}
