# myschool-api

This has all the apis related to school backend

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
PHP>=7.1
Composer
Laravel
```

### Installing

Please go to the root of your application and complete the following steps:

```
- Copy .env.example to .env
- Create empty database in MySQL and have the connection details added in .env file
- Update values for all mandatory application variables in .env
```

Run the following commands in Terminal in the root of the application

```
1) Run composer install 
For mac: 
alias php="/opt/cpanel/ea-php72/root/usr/bin/php"
php -d memory_limit=4G /opt/cpanel/composer/bin/composer install --no-scripts
2) setup .env file, add database details (copy all the items from .env.example)
3) php artisan key:generate
4) php artisan jwt:secret
5) php artisan storage:link  (create a symlink for storage)
```

This commands are to setup basic data

```
1) php artisan migrate:refresh
2) php artisan db:seed
3) php artisan role:permissions
```

6) php artisan cache:clear
7) php artisan view:clear
8) php artisan config:clear
9) php artisan route:clear

