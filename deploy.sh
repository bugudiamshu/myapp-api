
if [[ "$1" == "dev-api" ]]; then
    
  envoy run deploy-backend --branch=dev
  
  echo "Update of backend for dev completed"
fi

if [[ "$1" == "dev-frontend" ]]; then
    
  envoy run deploy-frontend --branch=dev
  
  echo "Update of frontend for dev completed"
fi

if [[ "$1" == "dev-deploy" ]]; then
    
  envoy run deploy-backend --branch=dev
  envoy run deploy-frontend --branch=dev
  
  echo "Update of backend and frontend for dev completed"
fi


if [[ "$1" == "prod-api" ]]; then
    
  envoy run deploy-backend
  
  echo "Update of backend for production completed"
fi

if [[ "$1" == "prod-frontend" ]]; then
    
  envoy run deploy-frontend
  
  echo "Update of frontend for production completed"
fi

if [[ "$1" == "prod-deploy" ]]; then
    
  envoy run deploy-backend
  envoy run deploy-frontend
  
  echo "Update of backend and frontend for production completed"
fi

if [[ "$1" == "all-api" ]]; then
    
  envoy run deploy-backend --branch=dev
  envoy run deploy-backend
  
  echo "Update of backend for all instances including production completed"
fi

if [[ "$1" == "all-frontend" ]]; then
    
  envoy run deploy-frontend --branch=dev
  envoy run deploy-frontend
  
  echo "Update of frontend for all instances including production completed"
fi